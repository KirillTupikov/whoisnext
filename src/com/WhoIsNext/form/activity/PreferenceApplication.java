package com.WhoIsNext.form.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.text.TextUtils;

import com.WhoIsNext.App;
import com.WhoIsNext.WhoIsNext.R;
import com.WhoIsNext.form.IDialogSupportedActivity;
import com.WhoIsNext.form.dialog.AppModalDialog;
import com.WhoIsNext.form.dialog.ButtonDialog;

/**
 * Created by kirill.tupikov on 10/4/13.
 */
public class PreferenceApplication extends PreferenceActivity
{

    public final static String PREFERENCE_APPLICATION = "preference_application";

    private AppModalDialog modalDialog;
    protected Handler handler = new Handler();

    //    private ListPreference listPreferenceLocale;
    private ListPreference listPreferenceCurrency;
    private EditTextPreference editTexMaxSum;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preference_application);

//        listPreferenceLocale = (ListPreference) findPreference("listLocaleApplication");
//        listPreferenceLocale.setOnPreferenceChangeListener(onPreferenceChangeListener);

        listPreferenceCurrency = (ListPreference) findPreference("listChangeApplication");
        listPreferenceCurrency.setOnPreferenceChangeListener(onPreferenceChangeListener);

        editTexMaxSum = (EditTextPreference) findPreference("editTexMaxSum");
        editTexMaxSum.setOnPreferenceChangeListener(onPreferenceChangeListener);


        SharedPreferences props = getSharedPreferences(App.PROPERTIES_FILE_NAME, Context.MODE_PRIVATE);
        if (props.contains(App.PROPERTY_MAX_SUM))
        {
            editTexMaxSum.setText(props.getString(App.PROPERTY_MAX_SUM, "0"));
        }
        if (props.contains(App.PROPERTY_CURRENCY))
        {
            listPreferenceCurrency.setValue(props.getString(App.PROPERTY_CURRENCY, "currency_bel"));
        }
    }


    private Preference.OnPreferenceChangeListener onPreferenceChangeListener = new Preference.OnPreferenceChangeListener()
    {
        @Override
        public boolean onPreferenceChange(Preference preference, Object o)
        {
            SharedPreferences.Editor editor = getSharedPreferences(App.PROPERTIES_FILE_NAME, Context.MODE_PRIVATE).edit();
            if (preference.equals(listPreferenceCurrency))
            {
                editor.putString(App.PROPERTY_CURRENCY, String.valueOf(o));
            }
            else
                if (preference.equals(editTexMaxSum))
                {
                    int value;
                    try
                    {
                        value = Integer.valueOf(String.valueOf(o));

                        if(value < 0)
                            return false;
                    }
                    catch (Exception e)
                    {
//                        showDialog(IDialogSupportedActivity.WARNING_DIALOG,
//                                getString(R.string.AuthorizationAct_InputAdminNameDialogTitle),
//                                getString(R.string.AuthorizationAct_InputAdminNameDialogDescription),
//                                new ButtonDialog(new Runnable()
//                                {
//                                    @Override
//                                    public void run()
//                                    {
//
//                                    }
//                                }, getString(R.string.continue_str)), null);
                        return false;
                    }
                    editor.putString(App.PROPERTY_MAX_SUM, String.valueOf(o));
                }
            
            editor.commit();

            return true;
        }
    };


//    @Override
//    public void showDialog(String dialogType, String title, String message, ButtonDialog actionPositive, ButtonDialog actionNegative)
//    {
//        if (modalDialog != null)
//        {
//            modalDialog.stop();
//        }
//
//        if (actionPositive != null && actionNegative != null)
//        {
//            modalDialog = new AppModalDialog(handler, this, dialogType, title, message, actionPositive, actionNegative);
//        }
//
//        runOnUiThread(modalDialog);
//    }
}
