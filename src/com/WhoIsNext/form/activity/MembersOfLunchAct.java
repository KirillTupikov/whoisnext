package com.WhoIsNext.form.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.WhoIsNext.ActivityLauncher;
import com.WhoIsNext.App;
import com.WhoIsNext.Settings;
import com.WhoIsNext.WhoIsNext.R;
import com.WhoIsNext.data.Lunch;
import com.WhoIsNext.data.User;
import com.WhoIsNext.data.UserLunch;
import com.WhoIsNext.gui.MemberOfLunchAdapter;
import com.WhoIsNext.managers.DataBaseManager;
import com.WhoIsNext.managers.ImageManager;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: kirill.tupikov
 * Date: 9/24/13
 * Time: 2:28 PM
 * To change this template use File | Settings | File Templates.
 */
public class MembersOfLunchAct extends BaseActivity
{
    public static final String LUNCH = "lunch";

    private ArrayList<UserLunch> usersLunches;

    private TextView namePayer;
    private TextView allSumPayment;
    private TextView numberOfCheck;
    private ListView listOfMembers;
    private ImageButton checkButton;
    private ImageView payerImageButton;

    private User adminLunch;

    private Lunch lunch;

    private MemberOfLunchAdapter memberOfLunchAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.members_of_lunch_act);

        lunch = null;
        try
        {
            lunch = DataBaseManager.getInstance().getLunchByIndex((Integer) getIntent().getSerializableExtra(LUNCH));
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        parseUsers(lunch);

        setUpControls();
    }

    private void parseUsers(Lunch lunch)
    {
        usersLunches = new ArrayList<UserLunch>();
        for (UserLunch userLunch : lunch.getUserLunches())
        {
            usersLunches.add(userLunch);
        }
    }

    private void setUpControls()
    {
        setTitleActionBar(getString(R.string.label_action_bar_members_of_lunch_act));

        allSumPayment = (TextView) findViewById(R.id.allSumPayment);
        allSumPayment.setText(String.valueOf(lunch.getSumLunch()) + App.getCurrency(context));

        listOfMembers = (ListView) findViewById(R.id.listOfMembers);

        createOrUpdateList();

        listOfMembers.setClickable(true);
        listOfMembers.setOnItemClickListener(onClickListenerListViewItem);

        checkButton = (ImageButton) findViewById(R.id.checkButton);
        checkButton.setOnClickListener(onClickButtonCheck);

        payerImageButton = (ImageView) findViewById(R.id.payerImageButton);
        payerImageButton.setOnClickListener(onClickButtonPayer);

        numberOfCheck = (TextView) findViewById(R.id.numberOfCheck);
        String check = getString(R.string.label_number_of_check, String.valueOf(lunch.getImageCheck().size()));
        numberOfCheck.setText(check);

        namePayer = (TextView) findViewById(R.id.namePayer);
        for (UserLunch userLunch : usersLunches)//Ищем плательщика за ланч
        {
            if (userLunch.getStatus().equals(User.USER_STATUS_ADMIN_LUNCH))
            {
                adminLunch = userLunch.getUser();
                namePayer.setText(userLunch.getUser().getUserContact().getUserName());
                //Bitmap bitmap = ImageManager.getInstance().loadContactPhoto(getContentResolver(), Long.valueOf(userLunch.getUser().getUserContact().getUserContactId()));
                Bitmap bitmap = ImageManager.getInstance().loadImageByPath(adminLunch.getUserContact().getUserPhoto());
                if (bitmap != null)
                    payerImageButton.setImageBitmap(ImageManager.getInstance().circleAvatar(bitmap));
                else
                    payerImageButton.setImageBitmap(ImageManager.getInstance().circleAvatar(BitmapFactory.decodeResource(getResources(), R.drawable.ic_person)));
                break;
            }
        }

    }

    private AdapterView.OnItemClickListener onClickListenerListViewItem = new AdapterView.OnItemClickListener()
    {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
        {
            ActivityLauncher.showUserStatisticsAct(context, lunch.getUserLunches().get(i).getUser().getId());
        }
    };

    private void createOrUpdateList()
    {
        memberOfLunchAdapter = new MemberOfLunchAdapter(this, usersLunches);
        listOfMembers.setAdapter(memberOfLunchAdapter);
    }

    private View.OnClickListener onClickButtonCheck = new View.OnClickListener()
    {

        @Override
        public void onClick(View view)
        {
            if (lunch.getImageCheck().size() != 0)
                ActivityLauncher.showSnapshotsActForResult(MembersOfLunchAct.this, Settings.ACTIVITY_SNAPSHOT_COD_REQUEST, lunch.getImageCheck(), SnapshotsAct.VIEW_MODE);
        }
    };

    private View.OnClickListener onClickButtonPayer = new View.OnClickListener()
    {

        @Override
        public void onClick(View view)
        {
            ActivityLauncher.showUserStatisticsAct(context, adminLunch.getId());
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (RESULT_OK == resultCode)
        {
            if (requestCode == Settings.ACTIVITY_SNAPSHOT_COD_REQUEST)//SnapshotAct
            {
                ArrayList<String> imageCheck = ((ArrayList<String>) data.getSerializableExtra(Lunch.IMAGE_CHECK_FIELD_NAME));
                //если изображения были удалены
                if (imageCheck.size() != lunch.getImageCheck().size())
                {
                    lunch.setImageCheck(imageCheck);
                    try
                    {
                        //обновляем ланч
                        DataBaseManager.getInstance().createLunch(lunch);
                    }
                    catch (SQLException e)
                    {
                        e.printStackTrace();
                    }
                    String check = getString(R.string.label_number_of_check, String.valueOf(lunch.getImageCheck().size()));
                    numberOfCheck.setText(check);
                }
            }

        }
    }

}

