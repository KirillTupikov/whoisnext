package com.WhoIsNext.form.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.WhoIsNext.WhoIsNext.R;

/**
 * Created by kirill.tupikov on 10/4/13.
 */
public class PreferenceAct extends BaseActivity
{
    public final static String PREFERENCE_ACTIVITY = "preference_activity";

    String preference;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.preference_act);

        preference = (String) getIntent().getStringExtra(PREFERENCE_ACTIVITY);

    }

    public boolean onCreateOptionsMenu(Menu menu)
    {
        if (preference.equals(PreferenceApplication.PREFERENCE_APPLICATION))
        {
            MenuItem mi = menu.add(0, 1, 0, "Preferences");
            mi.setIntent(new Intent(this, PreferenceApplication.class));
            return super.onCreateOptionsMenu(menu);
        }
        else
            if (preference.equals(PreferenceParty.PREFERENCE_PARTY))
            {
                MenuItem mi = menu.add(0, 1, 0, "Preferences");
                mi.setIntent(new Intent(this, PreferenceParty.class));
                return super.onCreateOptionsMenu(menu);
            }
        return false;
    }
}
