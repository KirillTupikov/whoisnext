package com.WhoIsNext.form.activity;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.WhoIsNext.WhoIsNext.R;
import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.FacebookError;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by kirill.tupikov on 10/17/13.
 */
public class FacebookLoginAct extends BaseActivity
{
    private Facebook facebook;
    private AsyncFacebookRunner asyncFacebookRunner;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.facebook_login_act);

        facebook = new Facebook(getString(R.string.app_id));
        asyncFacebookRunner = new AsyncFacebookRunner(facebook);

        sharedPreferences = getPreferences(MODE_PRIVATE);

        String access_token = sharedPreferences.getString("access_token", null);
        long expires = sharedPreferences.getLong("access_expires", 0);

        //etProfileInformation();
        if (access_token != null)
        {
            facebook.setAccessToken(access_token);
            Log.d("FB Sessions", "" + facebook.isSessionValid());
        }
        if (expires != 0)
        {
            facebook.setAccessExpires(expires);

            Bundle extras = getIntent().getExtras();
//            final String key = extras.getString(JSON);
//
//            //ключ для функции получения иформации
//            getFriendInformation(key);
//            //ключ для получения аватара
//            getAvatar(key);
            getProfileInformation();
        }
        if (expires == 0)
        {
            loginToFacebook();
        }
    }

    @SuppressWarnings("deprecation")
    public void loginToFacebook()
    {

        if (!facebook.isSessionValid())
        {
            facebook.authorize(this, new String[]{"email", "publish_stream",
                    "read_stream", "offline_access"},
                    new Facebook.DialogListener()
                    {

                        @Override
                        public void onCancel()
                        {
                            //функция отмены действия
                            Toast toast = Toast.makeText(FacebookLoginAct.this, "menu_setting_party", Toast.LENGTH_SHORT);
                            toast.show();
                        }

                        @Override
                        public void onComplete(Bundle values)
                        {
                            // сохраняем данные что бы не вводить по триста раз на день
                            // при входе заполняем
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString("access_token", facebook.getAccessToken());
                            editor.putLong("access_expires", facebook.getAccessExpires());
                            editor.commit();
                            getProfileInformation();
                        }

                        @Override
                        public void onError(DialogError error)
                        {
                            // функция ошибки
                            Toast toast = Toast.makeText(FacebookLoginAct.this, "menu_setting_party", Toast.LENGTH_SHORT);
                            toast.show();
                        }

                        @Override
                        public void onFacebookError(FacebookError fberror)
                        {
                            // функция фейсбучной ошибки
                            Toast toast = Toast.makeText(FacebookLoginAct.this, "menu_setting_party", Toast.LENGTH_SHORT);
                            toast.show();
                        }
                    });
        }
    }

    @SuppressWarnings("deprecation")
    public void getAvatar(String key)
    {

        asyncFacebookRunner.request(key + "/picture?type=large&redirect=false&", new AsyncFacebookRunner.RequestListener()
        {

            @Override
            public void onComplete(final String response, Object state)
            {

                runOnUiThread(new Runnable()
                {

                    @Override
                    public void run()
                    {

                        try
                        {
                            JSONObject profile = new JSONObject(response);
                            JSONObject data = profile.getJSONObject("data");
                            String url = data.getString("url");
                            //по заданому url скачиваем фотографию
//                            imageLoader.init(ImageLoaderConfiguration.createDefault(DetalsActivity.this));
//                            imageLoader.displayImage(url, imageView);
                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }
                    }
                });
            }

            @Override
            public void onFacebookError(FacebookError e, Object state)
            {

            }

            @Override
            public void onFileNotFoundException(FileNotFoundException e, Object state)
            {

            }

            @Override
            public void onIOException(IOException e, Object state)
            {

            }

            @Override
            public void onMalformedURLException(MalformedURLException e, Object state)
            {

            }
        });
    }


    /**
     * Забираем информацию списка друзей при помощи graph anpi
     */
    @SuppressWarnings("deprecation")
    public void getProfileInformation()
    {

        Bundle params = new Bundle();
        params.putString("fields", "name, picture");
        //посылаем запрос на вывод всех друзей с картинками и именами
        asyncFacebookRunner.request("me/friends", params, new AsyncFacebookRunner.RequestListener()
        {

            @Override
            public void onComplete(String response, Object state)
            {

                //json который приходит с сервера
                String json = response;
                try
                {
                    JSONObject profile = new JSONObject(json);
                    JSONArray data = profile.getJSONArray("data");
//                    for (i = 0; i < data.length(); i++)
//                    {
//                        // забираем данные из json
//                        final String name = data.getJSONObject(i).getString("name");
//                        final String id = data.getJSONObject(i).getString("id");
//                        JSONObject picture = data.getJSONObject(i).getJSONObject("picture");
//                        JSONObject picdata = picture.getJSONObject("data");
//                        final String url = picdata.getString("url");
//                        runOnUiThread(new Runnable()
//                        {
//
//                            @Override
//                            public void run()
//                            {
//
//                                //закидываем в массив все что получили
//                                HashMap<String, Object> hm;
//                                hm = new HashMap<String, Object>();
//                                hm.put(FIRST, name);
//                                hm.put(LAST, id);
//                                hm.put(IMAGE, url);
//                                myUsers.add(hm);
//                                //вытаскиваем для передачи в адаптер (так лучше не делать, мой косяк)
//                                String[] urls = new String[myUsers.size()];
//                                String[] names = new String[myUsers.size()];
//                                String[] indexcode = new String[myUsers.size()];
//                                for (int i = 0; i < myUsers.size(); i++)
//                                {
//                                    urls[i] = (String) myUsers.get(i).get(IMAGE);
//                                    names[i] = (String) myUsers.get(i).get(FIRST);
//                                    indexcode[i] = (String) myUsers.get(i).get(LAST);
//                                }
//                                //передаем в адаптер для распечатки
//                                listView.setAdapter(new FriendsAdapter(MainActivity.this, indexcode, names, urls));
//                            }
//                        });
//                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFacebookError(FacebookError e, Object state)
            {

            }

            @Override
            public void onFileNotFoundException(FileNotFoundException e, Object state)
            {

            }

            @Override
            public void onIOException(IOException e, Object state)
            {

            }

            @Override
            public void onMalformedURLException(MalformedURLException e, Object state)
            {

            }
        });
    }

    @SuppressWarnings("deprecation")
    public void getFriendInformation(String key)
    {

        //id + всеп поля которые мы хотим видеть
        asyncFacebookRunner.request(key + "?fields=id,name,gender,locale,birthday,hometown,languages,timezone&",
                new AsyncFacebookRunner.RequestListener()
                {

                    @Override
                    public void onComplete(final String response, Object state)
                    {

                        runOnUiThread(new Runnable()
                        {

                            @Override
                            public void run()
                            {

                                //снова json который возвращает сервер
                                String json = response;
                                try
                                {
                                    JSONObject profile = new JSONObject(json);
                                    // getting name of the user
                                    String json_name = profile.getString("name");
                                    String json_male = profile.getString("gender");
                                    String json_locale = profile.getString("locale");
                                    //
                                    // data.setText("Name: " + json_name + "\nMale: " + json_male + "\nLocale: "+ json_locale);
                                }
                                catch (JSONException e)
                                {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }

                    @Override
                    public void onFacebookError(FacebookError e, Object state)
                    {

                    }

                    @Override
                    public void onFileNotFoundException(FileNotFoundException e, Object state)
                    {

                    }

                    @Override
                    public void onIOException(IOException e, Object state)
                    {

                    }

                    @Override
                    public void onMalformedURLException(MalformedURLException e, Object state)
                    {

                    }
                });
    }
}
