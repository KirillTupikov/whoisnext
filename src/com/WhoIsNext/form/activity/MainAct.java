package com.WhoIsNext.form.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import com.WhoIsNext.ActivityLauncher;
import com.WhoIsNext.App;
import com.WhoIsNext.Settings;
import com.WhoIsNext.WhoIsNext.R;
import com.WhoIsNext.data.Group;
import com.WhoIsNext.data.User;
import com.WhoIsNext.data.UserContact;
import com.WhoIsNext.data.UserGroup;
import com.WhoIsNext.form.dialog.ButtonDialog;
import com.WhoIsNext.gui.PartyListAdapter;
import com.WhoIsNext.managers.DataBaseManager;
import com.WhoIsNext.managers.ImageManager;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by kirill.tupikov on 7/4/13.
 */
public class MainAct extends BaseActivity
{

    private ImageButton buttonAddParty;
    private EditText inputAddParty;
    private PartyListAdapter partyListAdapter;
    private ListView partyListView;
    private ArrayList<Group> parties;
    private int clickGroupId;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_act);

        App.getInstance();
        ImageManager.init();

        try
        {
            setUpControls();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }


    @SuppressLint("NewApi")
    private void setUpControls() throws SQLException
    {
        setTitleActionBar(getString(R.string.party));
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setHomeButtonEnabled(false);

        buttonAddParty = (ImageButton) findViewById(R.id.buttonAddParty);
        buttonAddParty.setOnClickListener(onClickButtonAddParty);

        inputAddParty = (EditText) findViewById(R.id.inputAddParty);
        partyListView = (ListView) findViewById(R.id.listViewParty);

        registerForContextMenu(partyListView);

        createList();

        setDefaultParam();
    }

    private void setDefaultParam()
    {
        SharedPreferences.Editor editor = getSharedPreferences(App.PROPERTIES_FILE_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(App.PROPERTY_CURRENCY, String.valueOf(Settings.CURRENCY_BEL));
        editor.putString(App.PROPERTY_MAX_SUM, String.valueOf(Settings.MAX_SUM));
        editor.commit();
    }

    public AdapterView.OnItemClickListener onClickListenerListViewItem = new AdapterView.OnItemClickListener()
    {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
        {
            ActivityLauncher.showPaymentLunchAct(context, parties.get(i).getId());
        }
    };


    private void createList() throws SQLException
    {
        parties = new ArrayList<Group>();
        partyListAdapter = new PartyListAdapter(this, parties);
        partyListView.setAdapter(partyListAdapter);

        partyListView.setClickable(true);
        partyListView.setOnItemClickListener(onClickListenerListViewItem);

        updateList();
    }

    private void updateList()
    {
        parties.clear();
        try
        {
            parties.addAll(DataBaseManager.getInstance().getAllGroup());
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        partyListAdapter.notifyDataSetChanged();
    }

    private View.OnClickListener onClickButtonAddParty = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            //Проверка на валидность названия группы
            if (inputAddParty.getText().toString().isEmpty())
            {
                showDialog(INFO_DIALOG,
                        getString(R.string.party),
                        getString(R.string.bad_name_party),
                        new ButtonDialog(new Runnable()
                        {
                            @Override
                            public void run()
                            {

                            }
                        }, getString(R.string.continue_str)), null);
            }
            else
            {

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(inputAddParty.getWindowToken(), 0);

                try
                {
                    Group group = new Group(inputAddParty.getText().toString(), Group.STATUS_PARTY_DISABLED);
                    DataBaseManager.getInstance().createGroup(group);

                    UserContact userContact = DataBaseManager.getInstance().getUserContactByIndexContact("0");

                    User user = new User(0, 0, User.USER_STATUS_ADMIN_LUNCH, userContact);

                    DataBaseManager.getInstance().createUser(user);
                    UserGroup userGroup = new UserGroup(user);
                    userGroup.setGroup(group);

                    DataBaseManager.getInstance().createUserGroup(userGroup);
                    //party.addUserGroup(userGroup);

                    createList();
                }
                catch (SQLException e)
                {
                    e.printStackTrace();
                }

                inputAddParty.setText("");
            }
        }
    };

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo)
    {
        getMenuInflater().inflate(R.menu.party_menu, menu);
        //setMenuBackground();
    }


    @Override
    public boolean onContextItemSelected(final MenuItem item)
    {
        clickGroupId = parties.get(((AdapterView.AdapterContextMenuInfo) item.getMenuInfo()).position).getId();
        switch (item.getItemId())
        {
            case R.id.menuHistory:
                ActivityLauncher.showHistoryLunchesAct(context, clickGroupId);

                break;

            case R.id.menuAddAvatar:
                Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, Settings.RESULT_LOAD_IMAGE);

                break;

            case R.id.menuDelete:
                showDialog(WARNING_DIALOG,
                        getString(R.string.delete_party),
                        getString(R.string.message_delete_party),
                        new ButtonDialog(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                try
                                {
                                    DataBaseManager.getInstance().deleteGroup(parties.get(((AdapterView.AdapterContextMenuInfo) item.getMenuInfo()).position));
                                    createList();
                                }
                                catch (SQLException e)
                                {
                                    e.printStackTrace();
                                }
                            }
                        }, getString(R.string.continue_str)),
                        new ButtonDialog(new Runnable()
                        {
                            @Override
                            public void run()
                            {

                            }
                        }, getString(R.string.cancel)));
                break;
        }
        return super.onContextItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Settings.RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data)
        {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            Bitmap bitmap = BitmapFactory.decodeFile(picturePath);
            if (bitmap == null)
            {
                return;
            }

            try
            {
                Group group = DataBaseManager.getInstance().getGroupByIndex(clickGroupId);
                ImageManager.getInstance().deleteImage(group.getGroupPhoto());//удаляем предыдущую
                group.setGroupPhoto(ImageManager.getInstance().saveGenerateFileUri(bitmap, context));
                DataBaseManager.getInstance().createGroup(group);
                createList();
            }
            catch (SQLException e)
            {
                e.printStackTrace();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }

        }
    }
}
