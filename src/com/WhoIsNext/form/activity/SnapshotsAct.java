package com.WhoIsNext.form.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;

import com.WhoIsNext.ActivityLauncher;
import com.WhoIsNext.App;
import com.WhoIsNext.Settings;
import com.WhoIsNext.WhoIsNext.R;
import com.WhoIsNext.data.Lunch;
import com.WhoIsNext.managers.ImageManager;

import java.io.*;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: kirill.tupikov
 * Date: 9/2/13
 * Time: 2:59 PM
 * To change this template use File | Settings | File Templates.
 */
public class SnapshotsAct extends BaseActivity
{
    public static final String MODE = "mode";

    public static final String EDIT_MODE = "edit_mode";
    public static final String VIEW_MODE = "view_mode";


    private ImageButton addSnapshotButtonImage;
    private TableLayout snapshotTable;
    private ArrayList<String> imageCheck;
    private String mode;
    private int maxSize;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.snapshots_act);

        Intent intent = getIntent();
        imageCheck = (ArrayList<String>) intent.getSerializableExtra(Lunch.IMAGE_CHECK_FIELD_NAME);
        mode = intent.getStringExtra(MODE);

        maxSize = App.getInstance().getMaximumSizeImage();

        setUpControl();
    }

    private void setUpControl()
    {
        setTitleActionBar(getString(R.string.label_action_bar_snapshot_act));


        addSnapshotButtonImage = (ImageButton) findViewById(R.id.addSnapshotButtonImage);

        snapshotTable = (TableLayout) findViewById(R.id.snapshotTable);

        if (mode.equals(EDIT_MODE))
        {
            addSnapshotButtonImage.setOnClickListener(onClickButtonAddSnapshotButtonImage);
        }
        else
        {
            addSnapshotButtonImage.setVisibility(View.GONE);
            TableRow row = (TableRow) snapshotTable.getChildAt(0);
            row.removeViewsInLayout(0, 1);
            snapshotTable.removeAllViews();
            snapshotTable.addView(row);
        }

        //Если коллекция пришла не пустая, то загружаем изображения на форму
        if (!imageCheck.isEmpty())
        {
            for (String fileName : imageCheck)
            {
                displayCheck(BitmapFactory.decodeFile(ImageManager.getInstance().getDirectory().getPath() + "/" + fileName), fileName);
            }
        }

    }

    private View.OnClickListener onClickButtonAddSnapshotButtonImage = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            if (App.getInstance().getMaximumSizeImage() == 0)
            {
                App.getInstance().setMaximumSizeImage(addSnapshotButtonImage.getWidth());
                maxSize = App.getInstance().getMaximumSizeImage();
            }

            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, Settings.REQUEST_CODE_PHOTO);
        }
    };

    private void deleteCheck()
    {
        TableRow row = (TableRow) snapshotTable.getChildAt(0);
        if (mode.equals(EDIT_MODE))
            row.removeViewsInLayout(1, 1);

        snapshotTable.removeAllViews();
        snapshotTable.addView(row);

        if (!imageCheck.isEmpty())
        {
            for (String fileName : imageCheck)
            {
                displayCheck(BitmapFactory.decodeFile(ImageManager.getInstance().getDirectory().getPath() + "/" + fileName), fileName);
            }
        }

    }

    private void displayCheck(Bitmap bitmap, String fileName)
    {
        TableRow row = (TableRow) snapshotTable.getChildAt(snapshotTable.getChildCount() - 1);
        //Если в последней стороке один элемент, то добавляем к ней ещё один, если же нет, то создаём новую строку и в неё же добавляем
        ImageView imageView;

        if (row.getChildCount() == 1)
        {
            LinearLayout linearLayout = new LinearLayout(context);
            linearLayout.setBackgroundResource(R.drawable.ic_photo_null);

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            linearLayout.setGravity(Gravity.CENTER);
            linearLayout.setLayoutParams(layoutParams);

            layoutParams.leftMargin = 10;

            imageView = new ImageView(context);
            imageView.setImageBitmap(ImageManager.getInstance().resizeImage(bitmap, maxSize));
            imageView.setOnClickListener(onClickImage);
            imageView.setTag(fileName);

            LinearLayout.LayoutParams layoutParamsImage = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            imageView.setLayoutParams(layoutParamsImage);

            linearLayout.addView(imageView);

            row.addView(linearLayout, new TableRow.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        }
        else
        {
            LinearLayout linearLayout = new LinearLayout(context);
            linearLayout.setBackgroundResource(R.drawable.ic_photo_null);

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.rightMargin = 10;
            linearLayout.setGravity(Gravity.CENTER);
            linearLayout.setLayoutParams(layoutParams);

            imageView = new ImageView(context);
            imageView.setImageBitmap(ImageManager.getInstance().resizeImage(bitmap, maxSize));
            imageView.setOnClickListener(onClickImage);
            imageView.setTag(fileName);

            LinearLayout.LayoutParams layoutParamsImage = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            imageView.setLayoutParams(layoutParamsImage);

            linearLayout.addView(imageView);

            TableRow.LayoutParams layoutParamsTable = new TableRow.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT);
            layoutParamsTable.rightMargin = 10;
            TableRow bufRow = new TableRow(context);
            bufRow.setOrientation(LinearLayout.HORIZONTAL);
            bufRow.setPadding(15, 15, 15, 0);

            bufRow.addView(linearLayout, layoutParamsTable);

            snapshotTable.addView(bufRow);
        }

        //ImageManager.getInstance().imageLoader(ImageManager.getInstance().getDirectory().getPath() + "/" + fileName, imageView, context);

    }

    private View.OnClickListener onClickImage = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            ActivityLauncher.showSnapshotViewActForResult(SnapshotsAct.this, Settings.ACTIVITY_SNAPSHOT_VIEW_COD_REQUEST, (String) view.getTag());
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent)
    {
        if (requestCode == Settings.REQUEST_CODE_PHOTO)
        {
            if (resultCode == RESULT_OK)
            {
                if (intent == null)
                {
                    Log.d(TAG, "Intent is null");
                }
                else
                {
                    Log.d(TAG, "Photo uri: " + intent.getData());
                    Bundle bndl = intent.getExtras();
                    if (bndl != null)
                    {
                        Object obj = intent.getExtras().get("data");
                        if (obj instanceof Bitmap)
                        {
                            Bitmap bitmap = (Bitmap) obj;
                            Log.d(TAG, "bitmap " + bitmap.getWidth() + " x " + bitmap.getHeight());

                            //пишем картинку
                            try
                            {
                                String path = ImageManager.getInstance().saveGenerateFileUri(bitmap, context);

                                imageCheck.add(path);
                                displayCheck(bitmap, path);
                            }
                            catch (IOException e)
                            {
                                e.printStackTrace();
                                Log.d(TAG, "Error save BitMap");
                            }
                        }
                    }
                }
            }
            else
                if (resultCode == RESULT_CANCELED)
                {
                    Log.d(TAG, "Canceled");
                }
        }
        else
            if (requestCode == Settings.ACTIVITY_SNAPSHOT_VIEW_COD_REQUEST)
            {
                if (resultCode == RESULT_OK)
                {
                    String deleteImagePath = intent.getStringExtra(Lunch.IMAGE_CHECK_FIELD_NAME);
                    for (String image : imageCheck)
                    {
                        if (image.equals(deleteImagePath))
                        {
                            imageCheck.remove(deleteImagePath);
                            break;
                        }
                    }

                    deleteCheck();
//                    snapshotTable = (TableLayout) findViewById(R.id.snapshotTable);
//                    setUpControl();
//                    //Если коллекция пришла не пустая, то загружаем изображения на форму
//                    if (!imageCheck.isEmpty())
//                    {
//                        for (String fileName : imageCheck)
//                        {
//                            displayCheck(BitmapFactory.decodeFile(ImageManager.getInstance().getDirectory().getPath() + "/" + fileName), fileName);
//                        }
//                    }
                }
            }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        Intent intent = new Intent();
        intent.putExtra(Lunch.IMAGE_CHECK_FIELD_NAME, imageCheck); //Возвращаем коллекцию названий чеков
        setResult(RESULT_OK, intent);
        return super.onKeyDown(keyCode, event);    //To change body of overridden methods use File | Settings | File Templates.
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {

        switch (item.getItemId())
        {

            case android.R.id.home:
                Intent intent = new Intent();
                intent.putExtra(Lunch.IMAGE_CHECK_FIELD_NAME, imageCheck); //Возвращаем коллекцию названий чеков
                setResult(RESULT_OK, intent);

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
