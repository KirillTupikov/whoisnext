package com.WhoIsNext.form.activity;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.*;

import com.WhoIsNext.App;
import com.WhoIsNext.WhoIsNext.R;
import com.WhoIsNext.data.User;
import com.WhoIsNext.managers.DataBaseManager;
import com.WhoIsNext.managers.ImageManager;

import java.sql.SQLException;

/**
 * Created by kirill.tupikov on 7/9/13.
 */
public class EditSumAct extends Activity
{
    public static final String USER_ITEM = "user_item";

    private User user;
    private VerticalSeekBar verticalSeekBar;
    private TextView labelMaxSum;
    private TextView labelMinSum;
    private TextView userName;
    private TextView labelCurrency;
    private EditText inputTextSum;
    private Button buttonReady;
    private ActionBar actionBar;
    private ImageView iconUser;
    private Context context;
    private double maxSum;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_sum_act);

        try
        {
            user = getUser(getIntent().getIntExtra(USER_ITEM, 0));
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        context = this;

        setUpControls();
    }

    private User getUser(int index) throws SQLException
    {
        return DataBaseManager.getInstance().getUserByIndex(index);
    }


    private void setUpControls()
    {
        iconUser = (ImageView) findViewById(R.id.iconUser);
        //Bitmap bitmap = ImageManager.getInstance().loadContactPhoto(getContentResolver(), Long.valueOf(user.getUserContact().getUserContactId()));
        Bitmap bitmap = ImageManager.getInstance().loadImageByPath(user.getUserContact().getUserPhoto());
        if (bitmap != null)
            iconUser.setImageBitmap(ImageManager.getInstance().circleAvatar(bitmap));
        else
            iconUser.setImageBitmap(ImageManager.getInstance().circleAvatar(BitmapFactory.decodeResource(getResources(), R.drawable.ic_person)));

        buttonReady = (Button) findViewById(R.id.buttonReady);
        buttonReady.setOnClickListener(onClickButtonReady);

        userName = (TextView) findViewById(R.id.userName);
        userName.setText(user.getUserContact().getUserName());
        labelMaxSum = (TextView) findViewById(R.id.labelMaxSum);
        labelMinSum = (TextView) findViewById(R.id.labelMinSum);
        inputTextSum = (EditText) findViewById(R.id.inputTextSum);
        labelCurrency = (TextView) findViewById(R.id.labelCurrency);
        labelCurrency.setText(App.getCurrency(context));

        verticalSeekBar = (VerticalSeekBar) findViewById(R.id.verticalSeekBar);

//        verticalSeekBar.setProgress(user.getDebtSum());
        inputTextSum.setText(String.valueOf(user.getDebtSum()));

        SharedPreferences props = getSharedPreferences(App.PROPERTIES_FILE_NAME, Context.MODE_PRIVATE);
        if (props.contains(App.PROPERTY_MAX_SUM))
        {
            labelMaxSum.setText(props.getString(App.PROPERTY_MAX_SUM, "0"));
            maxSum = Double.valueOf(props.getString(App.PROPERTY_MAX_SUM, "0")) / 100;
            verticalSeekBar.setProgress((int) (user.getDebtSum() / maxSum));
        }


        verticalSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
        {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b)
            {
                inputTextSum.setText(String.valueOf((int) (i * maxSum)));
                //user.setDebtSum((int) (i * maxSum));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar)
            {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar)
            {

            }
        });

    }

    private View.OnClickListener onClickButtonReady = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {

            Intent intent = new Intent();
            try
            {
                user.setDebtSum(Integer.valueOf(String.valueOf(inputTextSum.getText())));
            }
            catch (Exception e)
            {
                setResult(RESULT_CANCELED);
            }

            if (user.getDebtSum() == 0)
                setResult(RESULT_CANCELED);
            else
            {
                intent.putExtra(USER_ITEM, user);
                setResult(RESULT_OK, intent);
            }

            finish();
        }
    };
}
