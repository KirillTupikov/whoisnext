package com.WhoIsNext.form.activity;

import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;

import com.WhoIsNext.WhoIsNext.R;
import com.WhoIsNext.managers.ResourceManager;

import java.util.logging.LogManager;

/**
 * Created by Helm on 03.10.13.
 */
public class AboutProgramAct extends BaseActivity
{

    private WebView ctrlAboutDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_form_act);

        setTitle(R.string.AboutProgramAct_AboutTitle);

        ctrlAboutDescription = (WebView) findViewById(R.id.ctrlAboutDescription);
        ctrlAboutDescription.getSettings().setJavaScriptEnabled(true);
        if(Build.VERSION.SDK_INT >= 11) {
            ctrlAboutDescription.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }

        String version = "0.0";
        try {
            version = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            //Log.d("Error" + e.toString());
            e.printStackTrace();
        }

        String data = ResourceManager.readRawFile(this, R.raw.about);
        data = data.replace("{version}", version);
        setText(data);
    }

    private void setText(String text) {
        if (text != null) {
            ctrlAboutDescription.setBackgroundColor(Color.TRANSPARENT);
            ctrlAboutDescription.loadDataWithBaseURL("file:///android_res/raw/", text, "text/html", "UTF-8", null);
        }
    }
}
