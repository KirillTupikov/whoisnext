package com.WhoIsNext.form.activity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.WhoIsNext.Settings;
import com.WhoIsNext.WhoIsNext.R;
import com.WhoIsNext.managers.Account;
import com.perm.kate.api.Api;
import com.perm.kate.api.Auth;
import com.perm.kate.api.KException;

import org.json.JSONException;

import java.io.IOException;

public class VKontakteLoginAct extends BaseActivity
{
    //private static final String TAG = "Kate.VKontakteLoginAct";

    WebView webview;

    Account accountVK;
    Api apiVK;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        webview = (WebView) findViewById(R.id.vkontakteview);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.clearCache(true);

        //Восстановление сохранённой сессии
        accountVK = new Account();
        accountVK.restore(this);

        //Если сессия есть создаём API для обращения к серверу
        if (accountVK.getAccess_token() != null)
        {
            apiVK = new Api(accountVK.getAccess_token(), Settings.API_ID);
        }

        //Чтобы получать уведомления об окончании загрузки страницы
        webview.setWebViewClient(new VkontakteWebViewClient());

        //otherwise CookieManager will fall with java.lang.IllegalStateException: CookieSyncManager::createInstance() needs to be called before CookieSyncManager::getInstance()
//        CookieSyncManager.createInstance(this);
//
//        CookieManager cookieManager = CookieManager.getInstance();
//        cookieManager.removeAllCookie();

        String url = Auth.getUrl(Settings.API_ID, Auth.getSettings());
        webview.loadUrl(url);
    }

    class VkontakteWebViewClient extends WebViewClient
    {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon)
        {
            super.onPageStarted(view, url, favicon);
            parseUrl(url);
        }
    }

    private void parseUrl(String url)
    {
        try
        {
            if (url == null)
                return;
            Log.i(TAG, "url=" + url);
            if (url.startsWith(Auth.redirect_uri))
            {
                if (!url.contains("error="))
                {
                    String[] auth = Auth.parseRedirectUrl(url);

                    //авторизовались успешно

                    accountVK.setAccess_token(auth[0]);
                    accountVK.setUser_id(Long.parseLong(auth[1]));
                    accountVK.save(VKontakteLoginAct.this);
                    Api api = new Api(accountVK.getAccess_token(), Settings.API_ID);

//
//
//                    try
//                    {
//                        api.getFriends(accountVK.getUser_id(), null, null, null, null);
//                    }
//                    catch (IOException e)
//                    {
//                        e.printStackTrace();
//                    }
//                    catch (JSONException e)
//                    {
//                        e.printStackTrace();
//                    }
//                    catch (KException e)
//                    {
//                        e.printStackTrace();
//                    }
                }
                finish();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}