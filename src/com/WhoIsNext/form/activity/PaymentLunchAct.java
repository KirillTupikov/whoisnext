package com.WhoIsNext.form.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.WhoIsNext.ActivityLauncher;
import com.WhoIsNext.App;
import com.WhoIsNext.Settings;
import com.WhoIsNext.WhoIsNext.R;
import com.WhoIsNext.data.Group;
import com.WhoIsNext.data.Lunch;
import com.WhoIsNext.data.User;
import com.WhoIsNext.data.UserGroup;
import com.WhoIsNext.data.UserLunch;
import com.WhoIsNext.form.IDialogSupportedActivity;
import com.WhoIsNext.form.dialog.ButtonDialog;
import com.WhoIsNext.gui.LunchListAdapter;
import com.WhoIsNext.managers.DataBaseManager;
import com.WhoIsNext.managers.ImageManager;
import com.WhoIsNext.managers.Statistics;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by kirill.tupikov on 7/9/13.
 */
public class PaymentLunchAct extends BaseActivity
{
    public static final String PARTY_ITEM = "party_item";

    private ImageButton buttonAddUser;
    private ImageButton buttonAddCheck;
    private ImageButton buttonReady;
    private ImageButton buttonRefreshAdmin;
    private ImageView iconUser;
    private TextView inputSum;
    private TextView labelAddCheck;
    private ListView listViewUsers;

    private LunchListAdapter lunchListAdapter;

    private Group group;
    private ArrayList<User> users;
    private User administrator;
    private int allSumLunch;
    private Lunch lunch;
    private ArrayList<String> imageCheck;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment_lunch_act);

        try
        {
            loadGroupByIndex(getIntent().getIntExtra(PARTY_ITEM, 0));
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        setUpControls();
    }

    private void setUpControls()
    {
        setTitleActionBar(getString(R.string.users));

        buttonAddUser = (ImageButton) findViewById(R.id.buttonAddUser);
        buttonAddUser.setOnClickListener(onClickButtonAddUser);

        labelAddCheck = (TextView) findViewById(R.id.labelAddCheck);
        imageCheck = new ArrayList<String>();
        String check = getString(R.string.label_number_of_check, String.valueOf(imageCheck.size()));
        labelAddCheck.setText(check);

        buttonAddCheck = (ImageButton) findViewById(R.id.buttonAddCheck);
        buttonAddCheck.setOnClickListener(onClickButtonAddCheck);

        buttonReady = (ImageButton) findViewById(R.id.buttonReady);
        buttonReady.setOnClickListener(onClickButtonReady);

        buttonRefreshAdmin = (ImageButton) findViewById(R.id.buttonRefreshAdmin);
        buttonRefreshAdmin.setOnClickListener(onClickButtonRefreshAdmin);

        iconUser = (ImageView) findViewById(R.id.iconUser);

        inputSum = (TextView) findViewById(R.id.inputSum);

        listViewUsers = (ListView) findViewById(R.id.listViewUsers);

        registerForContextMenu(listViewUsers);

        createList();

        listViewUsers.setClickable(true);
        listViewUsers.setOnItemClickListener(onClickListenerListViewItem);

        inputSum.setText(getString(R.string.hint_all_sum_lunch) + updateAllSum() + App.getCurrency(context));//общая сумма

        //аватарка админа
        //Bitmap bitmap = ImageManager.getInstance().loadContactPhoto(getContentResolver(), Long.valueOf(administrator.getUserContact().getUserContactId()));
        Bitmap bitmap = ImageManager.getInstance().loadImageByPath(administrator.getUserContact().getUserPhoto());
        if (bitmap != null)
            iconUser.setImageBitmap(ImageManager.getInstance().circleAvatar(bitmap));
        else
            iconUser.setImageBitmap(ImageManager.getInstance().circleAvatar(BitmapFactory.decodeResource(getResources(), R.drawable.ic_person)));


    }

    private View.OnClickListener onClickButtonRefreshAdmin = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {

            showDialog(WARNING_DIALOG,
                    getString(R.string.lunch_payment),
                    getString(R.string.PaymentLaunchAct_RequestRefreshAdmin),
                    new ButtonDialog(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            //пересчет
                            String nextAdmin = "";
                            try
                            {
                                nextAdmin = Statistics.nextAdmin(group.getId(), true);//обновляем группу
                                loadGroupByIndex(group.getId());
                                setUpControls();
                            }
                            catch (SQLException e)
                            {
                                e.printStackTrace();
                                return;
                            }
                        }
                    }, getString(R.string.continue_str)),
                    new ButtonDialog(new Runnable()
                    {
                        @Override
                        public void run()
                        {

                        }
                    }, getString(R.string.cancel)));

        }
    };


    private void loadGroupByIndex(int groupIndex) throws SQLException
    {
        group = DataBaseManager.getInstance().getGroupByIndex(groupIndex);
    }


    private View.OnClickListener onClickButtonAddUser = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            ActivityLauncher.showAddUserActForResult(PaymentLunchAct.this, Settings.ACTIVITY_ADD_USER_COD_REQUEST, group.getId());
        }
    };

    private View.OnClickListener onClickButtonAddCheck = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            ActivityLauncher.showSnapshotsActForResult(PaymentLunchAct.this, Settings.ACTIVITY_SNAPSHOT_COD_REQUEST, imageCheck, SnapshotsAct.EDIT_MODE);
        }
    };

    private View.OnClickListener onClickButtonReady = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            //проверка, все ли ввели сумму
            for (User user : users)
            {
                if (!user.getStatus().equals(User.USER_STATUS_DISABLED) && user.getDebtSum() == 0)
                {
                    showDialog(WARNING_DIALOG,
                            getString(R.string.lunch_payment),
                            getString(R.string.not_all_filled),
                            new ButtonDialog(new Runnable()
                            {
                                @Override
                                public void run()
                                {

                                }
                            }, getString(R.string.continue_str)), null);

                    return;
                }
            }
            //Создаём ланч

            try
            {
                group = DataBaseManager.getInstance().getGroupByIndex(group.getId());

                //Создаём ланч
                final Calendar c = Calendar.getInstance();
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM-dd-yyyy HH:mm");
                String date = simpleDateFormat.format(c.getTimeInMillis());
                Date dateTime = null;
                try
                {
                    dateTime = simpleDateFormat.parse(date);
                }
                catch (ParseException e)
                {
                    e.printStackTrace();
                }

                lunch = new Lunch(dateTime, allSumLunch, group);
                lunch.setImageCheck(imageCheck);
                //

                lunch.setGroup(group);

                DataBaseManager.getInstance().createLunch(lunch);

                for (UserGroup userGroup : group.getUserGroups())
                {
                    UserLunch userLunch = new UserLunch(userGroup.getUser().getDebtSum(), userGroup.getUser().getStatus(), userGroup.getUser(), lunch);

                    try
                    {
                        //пишем в историю обед для каждого пользователя
                        DataBaseManager.getInstance().createUserLunch(userLunch);
                    }
                    catch (SQLException e)
                    {
                        Log.d(TAG, "Error create User lunch");
                        e.printStackTrace();
                    }

                    if (userGroup.getUser().getStatus().equals(User.USER_STATUS_ADMIN_LUNCH))//если это админ, то списываем ему сумму всего ланча
                    {
                        userGroup.getUser().setAllDebtSum(-allSumLunch);
                    }
                    else
                        if (userGroup.getUser().getStatus().equals(User.USER_STATUS_PAY))//если пользователь не заблочен
                            userGroup.getUser().setAllDebtSum(userGroup.getUser().getDebtSum());//суммируем

                    DataBaseManager.getInstance().createUser(userGroup.getUser());
                }
            }
            catch (SQLException e)
            {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                return;
            }

            String nextAdmin = "";
            try
            {
                nextAdmin = Statistics.nextAdmin(group.getId(), false);//обновляем группу
            }
            catch (SQLException e)
            {
                e.printStackTrace();
                return;
            }

            imageCheck.clear();

            showDialog(INFO_DIALOG,
                    getString(R.string.lunch_payment),
                    getString(R.string.lunchSuccessDescription) + nextAdmin,
                    new ButtonDialog(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            finish();
                        }
                    }, getString(R.string.continue_str)), null);

        }
    };

    public void updateList()
    {
        users.clear();

        for (UserGroup userGroup : group.getUserGroups())
        {
            if (userGroup.getUser().getStatus().equals(User.USER_STATUS_ADMIN_LUNCH))
            {
                administrator = userGroup.getUser();//добавляем администратора
            }

            users.add(userGroup.getUser());    //добавляем юзеров
        }

        lunchListAdapter.notifyDataSetChanged();
    }

    public void createList()
    {
        users = new ArrayList<User>();
        lunchListAdapter = new LunchListAdapter(this, R.layout.item_lunch, users);
        listViewUsers.setAdapter(lunchListAdapter);
        updateList();
    }

    public AdapterView.OnItemClickListener onClickListenerListViewItem = new AdapterView.OnItemClickListener()
    {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
        {
            if (!users.get(i).getStatus().equals(User.USER_STATUS_DISABLED))  //Если пользователь заблочен
                ActivityLauncher.showEditSumActForResult(PaymentLunchAct.this, users.get(i).getId(), Settings.ACTIVITY_EDIT_SUM_ITEM_MENU_COD_REQUEST);
        }
    };


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (RESULT_OK == resultCode)
        {
            if (requestCode == Settings.ACTIVITY_EDIT_SUM_ITEM_MENU_COD_REQUEST)
            {
                User user = (User) data.getSerializableExtra(EditSumAct.USER_ITEM);
                if (user.getStatus().equals(User.USER_STATUS_ADMIN_LUNCH))
                {
                    administrator = (User) data.getSerializableExtra(EditSumAct.USER_ITEM);
                }
                else
                    user.setStatus(User.USER_STATUS_PAY);
                try
                {
                    DataBaseManager.getInstance().createUser(user);  //Убрать, перенести в блок "Готово"
                    loadGroupByIndex(group.getId());
                }
                catch (SQLException e)
                {
                    e.printStackTrace();
                }

                updateList();
                allSumLunch = updateAllSum();
                inputSum.setText(getString(R.string.hint_all_sum_lunch) + String.valueOf(allSumLunch) + App.getCurrency(context));
                //lunchListAdapter.notifyDataSetChanged();
            }
            else
                if (requestCode == Settings.ACTIVITY_SNAPSHOT_COD_REQUEST)//SnapshotAct
                {
                    imageCheck.clear();
                    imageCheck.addAll((ArrayList<String>) data.getSerializableExtra(Lunch.IMAGE_CHECK_FIELD_NAME));
                    String check = getString(R.string.label_number_of_check, String.valueOf(imageCheck.size()));
                    labelAddCheck.setText(check);
                }
                else
                    if (requestCode == Settings.ACTIVITY_ADD_USER_COD_REQUEST)//AddUserAct
                    {
                        try
                        {
                            loadGroupByIndex((Integer) data.getSerializableExtra(Group.GROUP_ID));
                            updateList();
                        }
                        catch (SQLException e)
                        {
                            e.printStackTrace();
                        }
                        //Вытянуть обновления после добавления юзеров
                    }
        }
    }

    private int updateAllSum()
    {
        int sum = 0;
        for (User user : users)
        {
            sum = sum + user.getDebtSum();
        }
        return sum;
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo)
    {
        getMenuInflater().inflate(R.menu.lunch_members_menu, menu);
    }


    @Override
    public boolean onContextItemSelected(MenuItem item)
    {
        int index = ((AdapterView.AdapterContextMenuInfo) item.getMenuInfo()).position;
        switch (item.getItemId())
        {
            case R.id.menuSetAdminLunch:

                //смещаем прошлого админа
                if (administrator.getDebtSum() != 0)
                    administrator.setStatus(User.USER_STATUS_PAY);
                else
                    administrator.setStatus(User.USER_STATUS_NOT_PAY);
                try
                {
                    DataBaseManager.getInstance().createUser(administrator);//обновляем
                }
                catch (SQLException e)
                {
                    e.printStackTrace();
                }

                //Присвоение прав новому админу
                for (UserGroup userGroup : group.getUserGroups())
                {
                    if (userGroup.getUser().equals(users.get(index)))
                    {
                        users.get(index).setStatus(User.USER_STATUS_ADMIN_LUNCH);
                        administrator = users.get(index);
                        try
                        {
                            DataBaseManager.getInstance().createUser(administrator);
                            loadGroupByIndex(group.getId());
                            setUpControls();
                        }
                        catch (SQLException e)
                        {
                            e.printStackTrace();
                        }
                    }
                }

                break;

            case R.id.menuNonBlockingUser:

                for (UserGroup userGroup : group.getUserGroups())
                {
                    if (userGroup.getUser().equals(users.get(index)))
                    {
                        if (users.get(index).getDebtSum() != 0)
                            users.get(index).setStatus(User.USER_STATUS_PAY);
                        else
                            users.get(index).setStatus(User.USER_STATUS_NOT_PAY);

                        User user = users.get(index);

                        try
                        {
                            DataBaseManager.getInstance().createUser(user);
                        }
                        catch (SQLException e)
                        {
                            e.printStackTrace();
                        }
                    }
                }
                updateList();

                break;

            case R.id.menuBlockingUser:

                for (UserGroup userGroup : group.getUserGroups())
                {
                    if (userGroup.getUser().equals(users.get(index)))
                    {
                        users.get(index).setStatus(User.USER_STATUS_DISABLED);
                        User user = users.get(index);

                        try
                        {
                            DataBaseManager.getInstance().createUser(user);
                        }
                        catch (SQLException e)
                        {
                            e.printStackTrace();
                        }
                    }
                }
                updateList();

                break;

            case R.id.menuUserInfo:

                ActivityLauncher.showUserStatisticsAct(context, users.get(index).getId());

                break;

            case R.id.menuDeleteUser:
                //Удаляем объект связывающий группу и пользователя

                SharedPreferences props = context.getSharedPreferences(App.PROPERTIES_FILE_NAME, Context.MODE_PRIVATE);
                if (users.get(index).getUserContact().getId() == Integer.valueOf(props.getString(App.PROPERTY_ADMIN_ID, "")))//Нельзя удалить самого себя
                {
                    showDialog(WARNING_DIALOG,
                            getString(R.string.lunch_payment),
                            getString(R.string.PaymentLaunchAct_delete_main_admin_description),
                            new ButtonDialog(new Runnable()
                            {
                                @Override
                                public void run()
                                {

                                }
                            }, getString(R.string.continue_str)), null);
                }
                else
                    if (users.get(index).getStatus().equals(User.USER_STATUS_ADMIN_LUNCH))//Нельзя удалить администратора ланча
                    {
                        showDialog(WARNING_DIALOG,
                                getString(R.string.lunch_payment),
                                getString(R.string.PaymentLaunchAct_delete_admin_description),
                                new ButtonDialog(new Runnable()
                                {
                                    @Override
                                    public void run()
                                    {

                                    }
                                }, getString(R.string.continue_str)), null);
                    }
                    else
                    {
                        for (UserGroup userGroup : group.getUserGroups())
                        {
                            if (userGroup.getUser().equals(users.get(index)))
                            {
                                try
                                {
                                    DataBaseManager.getInstance().deleteUserGroup(userGroup);
                                    loadGroupByIndex(group.getId());//обновляем
                                }
                                catch (SQLException e)
                                {
                                    e.printStackTrace();
                                }
                                users.remove(index);

                                break;
                            }
                        }

                        updateList();
                    }
                break;
        }
        return super.onContextItemSelected(item);
    }

    @Override
    protected void onDestroy()
    {
        //Если чеки не добавлены в историю, то удаляем их из дериктории приложения
        if (!imageCheck.isEmpty())
        {
            for (String imagePath : imageCheck)
            {
                ImageManager.getInstance().deleteImage(imagePath);
            }
        }

        super.onDestroy();
    }

    @Override
    protected void onRestart()
    {
        super.onRestart();

        try
        {
            loadGroupByIndex(group.getId());
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        updateList();

        Bitmap bitmap = ImageManager.getInstance().loadImageByPath(administrator.getUserContact().getUserPhoto());
        if (bitmap != null)
            iconUser.setImageBitmap(ImageManager.getInstance().circleAvatar(bitmap));
        else
            iconUser.setImageBitmap(ImageManager.getInstance().circleAvatar(BitmapFactory.decodeResource(getResources(), R.drawable.ic_person)));
    }
}
