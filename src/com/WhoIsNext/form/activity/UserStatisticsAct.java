package com.WhoIsNext.form.activity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.WhoIsNext.App;
import com.WhoIsNext.WhoIsNext.R;
import com.WhoIsNext.data.User;
import com.WhoIsNext.managers.DataBaseManager;
import com.WhoIsNext.managers.ImageManager;
import com.WhoIsNext.managers.Statistics;

import java.sql.SQLException;

/**
 * Created by kirill.tupikov on 10/10/13.
 */
public class UserStatisticsAct extends BaseActivity
{
    private ImageView userAvatar;
    private TextView nameUser;
    private TextView userDebSum;
    private TextView averageDebSum;

    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.user_statistics_act);

        try
        {
            user = DataBaseManager.getInstance().getUserByIndex(getIntent().getIntExtra(User.USER_ID, 1));
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        setUpControl();
    }

    private void setUpControl()
    {
        setTitle(R.string.UserStatisticsAct_InfoUser);

        userAvatar = (ImageView) findViewById(R.id.userAvatar);
        //Bitmap bitmap = ImageManager.getInstance().loadContactPhoto(getContentResolver(), Long.valueOf(user.getUserContact().getUserContactId()));
        Bitmap bitmap = ImageManager.getInstance().loadImageByPath(user.getUserContact().getUserPhoto());
        if (bitmap != null)
            userAvatar.setImageBitmap(bitmap);
        else
            userAvatar.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.ic_person));

        nameUser = (TextView) findViewById(R.id.nameUser);
        nameUser.setText(user.getUserContact().getUserName());

        userDebSum = (TextView) findViewById(R.id.userDebSum);
        userDebSum.setText(user.getAllDebtSum() + App.getCurrency(context));

        averageDebSum = (TextView) findViewById(R.id.averageDebSum);
        averageDebSum.setText(Statistics.getAverageSumLunchUser(user) + App.getCurrency(context));
    }
}
