package com.WhoIsNext.form.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.Html;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import com.WhoIsNext.App;
import com.WhoIsNext.WhoIsNext.R;
import com.WhoIsNext.data.Group;
import com.WhoIsNext.data.Lunch;
import com.WhoIsNext.data.User;
import com.WhoIsNext.data.UserContact;
import com.WhoIsNext.data.UserGroup;
import com.WhoIsNext.gui.UserContactsAdapter;
import com.WhoIsNext.managers.DataBaseManager;
import com.WhoIsNext.managers.ImageManager;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: kirill.tupikov
 * Date: 9/27/13
 * Time: 3:45 PM
 * To change this template use File | Settings | File Templates.
 */
public class AddUserAct extends BaseActivity implements SearchView.OnQueryTextListener, SearchView.OnCloseListener
{

    private TextView userGroup;
    private ImageView listUsersRefreshIcon;

    private ArrayAdapter<UserContact> userContactsAdapter;
    private ArrayList<UserContact> userContacts;
    private ArrayList<UserContact> actualUsersContact;
    private PullToRefreshListView mPullRefreshListView;
    private ListView actualListView;

    private SearchView searchView;

    private Group group;

    SharedPreferences props;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.add_user_act);

        try
        {
            group = DataBaseManager.getInstance().getGroupByIndex(getIntent().getIntExtra(Group.GROUP_ID, 0));
        }
        catch (SQLException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        props = context.getSharedPreferences(App.PROPERTIES_FILE_NAME, Context.MODE_PRIVATE);

        mPullRefreshListView = (PullToRefreshListView) findViewById(R.id.pull_refresh_list);

        // Set a listener to be invoked when the list should be refreshed.
        mPullRefreshListView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>()
        {
            @Override
            public void onRefresh(PullToRefreshBase<ListView> refreshView)
            {
                String label = DateUtils.formatDateTime(getApplicationContext(), System.currentTimeMillis(),
                        DateUtils.FORMAT_SHOW_TIME | DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_ABBREV_ALL);

                // Update the LastUpdatedLabel
                refreshView.getLoadingLayoutProxy().setLastUpdatedLabel(label);
                // Do work to refresh the list here.

                new GetDataTask().execute();

            }
        });

        // Add an end-of-list listener
        mPullRefreshListView.setOnLastItemVisibleListener(new PullToRefreshBase.OnLastItemVisibleListener()
        {
            @Override
            public void onLastItemVisible()
            {
                //Toast.makeText(NewsList.this, R.string.end_of_list, Toast.LENGTH_SHORT).show();////
            }
        });

        setUpControl();
    }

    private void setUpControl()
    {
        setTitle(R.string.label_add_user_act);

        userGroup = (TextView) findViewById(R.id.userGroup);
        userGroup.setText(group.getGroupName());

        listUsersRefreshIcon = (ImageView) findViewById(R.id.listUsersRefreshIcon);

//        AutoCompleteTextView searchText = (AutoCompleteTextView) searchView.findViewById(R.id.search);
//        searchText.setHintTextColor(getResources().getColor(R.color.white));
//        searchText.setTextColor(getResources().getColor(R.color.white));

        searchView = (SearchView) findViewById(R.id.search);
        //searchView.setQueryHint(Html.fromHtml("<font color = #ffffff>" + getResources().getString(R.string.search_hint) + "</font>"));
        searchView.setIconifiedByDefault(true);

        searchView.setOnQueryTextListener(this);
        searchView.setOnCloseListener(this);

        createActualUserContacts();
        createList();
        loadContacts();

        if (userContacts.isEmpty())
        {
            listUsersRefreshIcon.setVisibility(View.VISIBLE);
        }
        else
        {
            listUsersRefreshIcon.setVisibility(View.GONE);
        }

        actualListView.setClickable(true);
        actualListView.setOnItemClickListener(onClickListenerListViewItem);
    }

    private void loadContacts()
    {
        userContacts.clear();
        try
        {
            userContacts.addAll(DataBaseManager.getInstance().getAllUserContact());
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        setCheckActualUserContact();

        userContactsAdapter.notifyDataSetChanged();
    }

    private void setCheckActualUserContact()
    {

        //Если пользователь уже добавлен в нашу группу, то выставляем ему флаг

        for (UserContact aUserContact: actualUsersContact)
        {
            for (UserContact userContact : userContacts)
            {
                if (userContact.getUserContactId().equals(aUserContact.getUserContactId()))
                {
                    userContact.setAddedToGroup(true);
                    if (String.valueOf(aUserContact.getId()).equals(props.getString(App.PROPERTY_ADMIN_ID, "")))//удаляем админа из списка контактов
                    {
                        userContacts.remove(userContact);
                    }
                    break;
                }
            }
        }
    }

    private void createActualUserContacts()
    {
        actualUsersContact = new ArrayList<UserContact>();

        for (UserGroup userGroup : group.getUserGroups())
        {
            actualUsersContact.add(userGroup.getUser().getUserContact());
        }
    }

    private void changeActualUserContacts(UserContact userContact)
    {
        if(userContact.isAddedToGroup())
            actualUsersContact.add(userContact);
        else
            actualUsersContact.remove(userContact);
    }

    private void createList()
    {
        userContacts = new ArrayList<UserContact>();

        actualListView = mPullRefreshListView.getRefreshableView();
        userContactsAdapter = new UserContactsAdapter(context, R.layout.item_user_contacts, userContacts);
        actualListView.setAdapter(userContactsAdapter);
    }

    public AdapterView.OnItemClickListener onClickListenerListViewItem = new AdapterView.OnItemClickListener()
    {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
        {
            userContacts.get(i - 1).clickAddedToGroup();

            //Добавляем или удаляем пользователя из группы
            changeActualUserContacts(userContacts.get(i - 1));

            userContactsAdapter.notifyDataSetChanged();
        }
    };

    public ArrayList<UserContact> getAllContacts(Context context)
    {
        ArrayList<UserContact> contacts = new ArrayList<UserContact>();
        UserContact contact;
        Cursor cursor = context.getContentResolver().query(
                ContactsContract.Contacts.CONTENT_URI,
                null,
                null,
                null,
                null
        );

        if (cursor.getCount() > 0)
        {
            while (cursor.moveToNext())
            {
                contact = new UserContact();

                final String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                contact.setUserContactId(id);

                String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                contact.setUserName(name);

                String photo = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.PHOTO_URI));
                contact.setUserPhoto(photo);

                if (Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0)
                {
                    Log.i(TAG, "Contact name=" + name + ", Id=" + id);
                    // get the phone number
                    Cursor pCur = context.getContentResolver().query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{id},
                            null);
                    while (pCur.moveToNext())
                    {
                        String phone = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        contact.setUserPhone(phone);
//                        String label= HelpUtils.getPhoneLabel(context, pCur.getInt(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE)),
//                                pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.LABEL)));
//                        PhoneNumber phoneNumber = new PhoneNumber();
//                        phoneNumber.setPhoneNumber(phone);
//                        phoneNumber.setType(label);
//                        contact.addPhoneNumber(phoneNumber);
                        Log.i(TAG, "phone=" + phone);
                    }
                    pCur.close();
                    contacts.add(contact);
                    try
                    {
                        boolean f = true;//Если в существующем списке контактов нету полученного контакта, то пишем его в базу
                        for (UserContact userContact : userContacts)
                        {
                            if (userContact.getUserContactId().equals(contact.getUserContactId()))
                            {
                                f = false;
                                break;
                            }
                        }
                        if (f)
                        {
                            DataBaseManager.getInstance().createUserContacts(contact);
                            userContacts.add(contact);
                        }
                    }
                    catch (SQLException e)
                    {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }
            }
        }
        return contacts;
    }

    @Override
    public boolean onClose()
    {
        loadContacts();
        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query)
    {
        displayResults(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText)
    {
        if (!newText.isEmpty())
        {
            displayResults(newText);
        }
        else
        {
            loadContacts();
        }
        return false;
    }

    private void displayResults(String query)
    {
        userContacts.clear();
        try
        {
            userContacts.addAll(DataBaseManager.getInstance().getUserContactsByInputText(query));
            setCheckActualUserContact();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        userContactsAdapter.notifyDataSetChanged();
    }


    private class GetDataTask extends AsyncTask<Void, Void, String[]>
    {
        @Override
        protected String[] doInBackground(Void... params)
        {
            userContacts = getAllContacts(context);

            return new String[0];
        }

        @Override
        protected void onPostExecute(String[] strings)
        {
            handler.post(afterLoadContacts);

            mPullRefreshListView.onRefreshComplete();

            if (userContacts.isEmpty())
            {
                listUsersRefreshIcon.setVisibility(View.VISIBLE);
            }
            else
            {
                listUsersRefreshIcon.setVisibility(View.GONE);
            }

            super.onPostExecute(strings);
        }
    }

    private Runnable afterLoadContacts = new Runnable()
    {
        @Override
        public void run()
        {
            createList();
            loadContacts();
        }
    };


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        searchNewUsers();
        return super.onKeyDown(keyCode, event);
    }

    private void searchNewUsers()
    {
        ArrayList<UserContact> contacts = new ArrayList<UserContact>();
        for (UserContact userContact : userContacts)
        {
            //добавляем все добавленные в группу контакты
            if (userContact.isAddedToGroup())
            {
                contacts.add(userContact);
            }
        }
        int i = 0;
        boolean f = true;
        //просматриваем, всели пользователи остались в группе
        for (UserGroup userGroup : group.getUserGroups())
        {
            f = true;
            i = 0;
            for (UserContact userContact : contacts)
            {
                if (userContact.getUserContactId().equals(userGroup.getUser().getUserContact().getUserContactId()))
                {
                    f = false;
                    contacts.get(i).clickAddedToGroup();
                    break;
                }
                i++;
            }
            //если не нашли в новом списке пользователя, то удаляем его их группы
            if (f)
            {
                try
                {
                    if (!String.valueOf(userGroup.getUser().getUserContact().getId()).equals(props.getString(App.PROPERTY_ADMIN_ID, "")))
                    {
                        DataBaseManager.getInstance().deleteUserGroup(userGroup);
                    }
                }
                catch (SQLException e)
                {
                    e.printStackTrace();
                }
            }
        }

        //проверяем на наличие недобавленных пользователей в группу
        for (UserContact userContact : contacts)
        {
            if (userContact.isAddedToGroup())
            {
                addUserContact(userContact);
            }
        }

        Intent intent = new Intent();
        intent.putExtra(Group.GROUP_ID, group.getId());
        setResult(RESULT_OK, intent);
    }

    private void addUserContact(UserContact userContact)
    {
        try
        {
            Bitmap phPhoto = ImageManager.getInstance().loadContactPhoto(context.getContentResolver(), Long.valueOf(userContact.getUserContactId()));
            if (phPhoto != null)
            {
                userContact.setUserPhoto(ImageManager.getInstance().saveGenerateFileUri(phPhoto, context));//сохраняем фото в директории приложения
            }

            DataBaseManager.getInstance().createOrUpdateUserContacts(userContact);

            User user = new User(0, 0, User.USER_STATUS_NOT_PAY, userContact);

            DataBaseManager.getInstance().createUser(user);
            UserGroup userGroup = new UserGroup(user);

            DataBaseManager.getInstance().createUserGroup(userGroup);
            group.addUserGroup(userGroup);

            DataBaseManager.getInstance().createGroup(group);//Обновляем данные в группе

        }
        catch (SQLException e)
        {
            Log.d(TAG, "Error create User Group");
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {

        switch (item.getItemId())
        {

            case android.R.id.home:
                searchNewUsers();

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
