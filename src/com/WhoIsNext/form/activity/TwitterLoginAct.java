package com.WhoIsNext.form.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.WhoIsNext.App;
import com.WhoIsNext.WhoIsNext.R;
import com.WhoIsNext.managers.ImageManager;

import oauth.signpost.OAuth;
import oauth.signpost.OAuthConsumer;
import oauth.signpost.OAuthProvider;
import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;
import oauth.signpost.commonshttp.CommonsHttpOAuthProvider;
import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.auth.AccessToken;
import twitter4j.conf.ConfigurationBuilder;

/**
 * Created by Helm on 18.10.13.
 */
public class TwitterLoginAct extends BaseActivity
{

    private static final String APP = "TWITTEROAUTH";

    private Twitter twitter;
    private OAuthProvider provider;
    private OAuthConsumer consumer;

    private String CONSUMER_KEY = "ojHOWh6zotRH0oaAk0ZQ";
    private String CONSUMER_SECRET = "72wlo9xVmEqyrhJ8DpfUWlSvDOQp7gl25FRygrlKlQ";
    private String CALLBACK_URL = "x-oauthflow-twitter://callback";

//    private TextView tweetTextView;
//    private Button buttonLogin;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.twitter_login_act);

        //if (checkNetworkStatus(true))
            new DownloadFilesTask().execute(null, null, null);
    }


    private class DownloadFilesTask extends AsyncTask<Void, Void, Void>
    {


        @Override
        protected Void doInBackground(Void... voids)
        {
            askOAuth();
            return null;
        }
    }

    private void askOAuth()
    {
        try
        {
            // Use Apache HttpClient for HTTP messaging
            consumer = new CommonsHttpOAuthConsumer(CONSUMER_KEY, CONSUMER_SECRET);
            provider = new CommonsHttpOAuthProvider("https://api.twitter.com/oauth/request_token",
                    "https://api.twitter.com/oauth/access_token",
                    "https://api.twitter.com/oauth/authorize");
            String authUrl = provider.retrieveRequestToken(consumer, CALLBACK_URL);
            //Toast.makeText(this, "Authorize this app!", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(authUrl)).setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_FROM_BACKGROUND);
            context.startActivity(intent);
            //finish();
        }
        catch (Exception e)
        {
            Log.e(APP, e.getMessage());
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }


    private class RetrieveAccessTokenTask extends AsyncTask<Uri, Void, Void>
    {
        @Override
        protected void onCancelled(Void aVoid)
        {

            super.onCancelled(aVoid);
        }

        @Override
        protected Void doInBackground(Uri... uri)
        {
            String verifier = uri[0].getQueryParameter(oauth.signpost.OAuth.OAUTH_VERIFIER);

            try
            {
                // Populate token and token_secret in consumer
                provider.retrieveAccessToken(consumer, verifier);

                // TODO: you might want to store token and token_secret in you app settings!
                AccessToken accessToken = new AccessToken(consumer.getToken(), consumer.getTokenSecret());

                // Initialize Twitter4J
                ConfigurationBuilder confbuilder = new ConfigurationBuilder();
                confbuilder.setOAuthAccessToken(accessToken.getToken())
                        .setOAuthAccessTokenSecret(accessToken.getTokenSecret())
                        .setOAuthConsumerKey(CONSUMER_KEY)
                        .setOAuthConsumerSecret(CONSUMER_SECRET);
                twitter = new TwitterFactory(confbuilder.build()).getInstance();

//                // Create a tweet
//                Date d = new Date(System.currentTimeMillis());
//                String tweet = "Twitter API 24 HRS: TwitterOAuth works on Android " + d.toLocaleString();

//                // Post the tweet
//                twitter.updateStatus(tweet);

                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
                final SharedPreferences.Editor edit = prefs.edit();
                edit.putString(OAuth.OAUTH_TOKEN, consumer.getToken());
                edit.putString(OAuth.OAUTH_TOKEN_SECRET, consumer.getTokenSecret());
                edit.commit();

                //пишем данные о пользователе
                long userID = accessToken.getUserId();
                User user = twitter.showUser(userID);

                SharedPreferences.Editor editor = context.getSharedPreferences(App.PROPERTIES_FILE_NAME, Context.MODE_PRIVATE).edit();

                editor.putString(App.PROPERTY_ADMIN_ID, String.valueOf(user.getId()));

                editor.putString(App.PROPERTY_ADMIN_NAME_APP, user.getName());

                String imagePath = ImageManager.getInstance().saveImageFromUri(user.getProfileImageURL(), context);
                editor.putString(App.PROPERTY_ADMIN_APP_AVATAR, imagePath);

                editor.commit();
                //twitter.getProfileImage(twitter.getScreenName(), Imagesize);

            }
            catch (Exception e)
            {
                Log.e(APP, e.getMessage());
                // Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
            }
            return null;
        }
    }


    /**
     * Get the verifier from the callback URL.
     * Retrieve token and token_secret.
     * Feed them to twitter4j along with consumer key and secret
     */
    @Override
    protected void onNewIntent(Intent intent)
    {

        super.onNewIntent(intent);

//        Uri uri = intent.getData();
//        if (uri != null && uri.toString().startsWith(CALLBACK_URL))
//        {
//            new RetrieveAccessTokenTask().execute(uri, null, null);
//        }


        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        final Uri uri = intent.getData();
        if (uri != null && uri.getScheme().equals(CALLBACK_URL))
        {
            Log.i(TAG, "Callback received : " + uri);
            Log.i(TAG, "Retrieving Access Token");
            new RetrieveAccessTokenTask().execute(uri);
            finish();
        }
    }
}
