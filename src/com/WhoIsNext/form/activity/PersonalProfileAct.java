package com.WhoIsNext.form.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.WhoIsNext.App;
import com.WhoIsNext.Settings;
import com.WhoIsNext.WhoIsNext.R;
import com.WhoIsNext.data.UserContact;
import com.WhoIsNext.managers.DataBaseManager;
import com.WhoIsNext.managers.ImageManager;
import com.WhoIsNext.managers.Statistics;

import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by kirill.tupikov on 10/22/13.
 */
public class PersonalProfileAct extends BaseActivity
{

    private ImageView userAvatar;
    private Button changeUserAvatar;
    private EditText userNikName;
    private TextView allDebSum;
    private TextView averageDebSum;
    private TextView debSumLastMonth;

    UserContact userContact;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.personal_profile_act);

        String index = getIntent().getStringExtra(UserContact.USER_CONTACT_ID_FIELD_NAME);
        try
        {
            userContact = DataBaseManager.getInstance().getUserContactByIndex(index);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        setUI();
    }

    private void setUI()
    {

        setTitle(R.string.PersonalProfileAct_TitleProfile);

        userAvatar = (ImageView) findViewById(R.id.userAvatar);
        //Bitmap bitmap = ImageManager.getInstance().loadContactPhoto(getContentResolver(), Long.valueOf(user.getUserContact().getUserContactId()));
        Bitmap bitmap = ImageManager.getInstance().loadImageByPath(userContact.getUserPhoto());
        if (bitmap != null)
            userAvatar.setImageBitmap(bitmap);
        else
            userAvatar.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.ic_person));

        changeUserAvatar = (Button) findViewById(R.id.changeUserAvatar);
        changeUserAvatar.setOnClickListener(clickChangeUserAvatar);

        userNikName = (EditText) findViewById(R.id.userNikName);
        userNikName.setText(userContact.getUserName());

        allDebSum = (TextView) findViewById(R.id.allDebSum);
        allDebSum.setText(Statistics.getAllGroupDebSum(userContact) + App.getCurrency(context));

        averageDebSum = (TextView) findViewById(R.id.averageDebSum);
        averageDebSum.setText(Statistics.getAverageSumAllLunchUser(userContact) + App.getCurrency(context));

        debSumLastMonth = (TextView) findViewById(R.id.debSumLastMonth);
        debSumLastMonth.setText(Statistics.getDebSumLastMonth(userContact) + App.getCurrency(context));
    }

    private View.OnClickListener clickChangeUserAvatar = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {

            Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(i, Settings.RESULT_LOAD_IMAGE);
        }
    };

    @Override
    protected void onStop()
    {
        //обновить данные о имени пользователя
        if (!String.valueOf(userNikName.getText()).isEmpty())
        {
            try
            {
                //Обновляем данные в базе
                UserContact userContactBuf = DataBaseManager.getInstance().getUserContactByIndexContact(String.valueOf(userContact.getUserContactId()));
                userContactBuf.setUserName(String.valueOf(userNikName.getText()));
                DataBaseManager.getInstance().createOrUpdateUserContacts(userContactBuf);

            }
            catch (SQLException e)
            {
                e.printStackTrace();
            }

            SharedPreferences props = context.getSharedPreferences(App.PROPERTIES_FILE_NAME, Context.MODE_PRIVATE);
            if (props.contains(App.PROPERTY_ADMIN_ID))
            {
                if (String.valueOf(userContact.getId()).equals(props.getString(App.PROPERTY_ADMIN_ID, "")))//Если это админ
                {
                    SharedPreferences.Editor editor = context.getSharedPreferences(App.PROPERTIES_FILE_NAME, Context.MODE_PRIVATE).edit();
                    editor.putString(App.PROPERTY_ADMIN_NAME_APP, String.valueOf(userNikName.getText()));
                    editor.commit();
                }
            }
        }

        super.onStop();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Settings.RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data)
        {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            Bitmap bitmap = BitmapFactory.decodeFile(picturePath);
            if(bitmap == null)
            {
                return;
            }
            userAvatar.setImageBitmap(bitmap);

            try
            {
                ImageManager.getInstance().deleteImage(userContact.getUserPhoto());
                userContact.setUserPhoto(ImageManager.getInstance().saveGenerateFileUri(BitmapFactory.decodeFile(picturePath), context));
                DataBaseManager.getInstance().createOrUpdateUserContacts(userContact);//Обновляем аватарку в базе
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            catch (SQLException e)
            {
                e.printStackTrace();
            }

        }
    }
}
