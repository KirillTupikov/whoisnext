package com.WhoIsNext.form.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.WhoIsNext.ActivityLauncher;
import com.WhoIsNext.WhoIsNext.R;
import com.WhoIsNext.data.Group;
import com.WhoIsNext.data.Lunch;
import com.WhoIsNext.gui.HistoryLunchesAdapter;
import com.WhoIsNext.managers.DataBaseManager;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: kirill.tupikov
 * Date: 9/23/13
 * Time: 2:49 PM
 * To change this template use File | Settings | File Templates.
 */
public class HistoryLunchesAct extends BaseActivity
{
    public final static String HISTORY_LUNCHES_GROUP = "history_lunches_group";

    private ListView listViewHistoryLunches;
    private TextView labelGroupName;
    private HistoryLunchesAdapter historyLunchesAdapter;
    private ArrayList<Lunch> lunches;
    private Group group;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.history_lunches_act);

        try
        {
            group = DataBaseManager.getInstance().getGroupByIndex((Integer) getIntent().getSerializableExtra(HISTORY_LUNCHES_GROUP));
        }
        catch (SQLException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        setUpControl();
    }

    private void setUpControl()
    {
        setTitleActionBar(getString(R.string.label_action_bar_history_lunches_act));

        labelGroupName = (TextView) findViewById(R.id.labelGroupName);
        labelGroupName.setText(group.getGroupName());

        listViewHistoryLunches = (ListView) findViewById(R.id.listViewHistoryLunches);

        createOrUpdateList();

        listViewHistoryLunches.setClickable(true);
        listViewHistoryLunches.setOnItemClickListener(onClickListenerListViewItem);

    }

    private void createOrUpdateList()
    {
        try
        {
            lunches = DataBaseManager.getInstance().getLunchesByGroup(group);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            Log.d(TAG, "Error load list of Lunches by Group id");
        }

        historyLunchesAdapter = new HistoryLunchesAdapter(this, lunches);
        listViewHistoryLunches.setAdapter(historyLunchesAdapter);
    }

    public AdapterView.OnItemClickListener onClickListenerListViewItem = new AdapterView.OnItemClickListener()
    {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
        {
            ActivityLauncher.showMembersOfLunchAct(context, lunches.get(i).getId());
        }
    };
}
