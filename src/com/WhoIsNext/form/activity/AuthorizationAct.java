package com.WhoIsNext.form.activity;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;

import com.WhoIsNext.ActivityLauncher;
import com.WhoIsNext.App;
import com.WhoIsNext.WhoIsNext.R;
import com.WhoIsNext.data.User;
import com.WhoIsNext.data.UserContact;
import com.WhoIsNext.form.IDialogSupportedActivity;
import com.WhoIsNext.form.dialog.ButtonDialog;
import com.WhoIsNext.managers.DataBaseManager;

import java.sql.SQLException;

/**
 * Created by Helm on 15.10.13.
 */
public class AuthorizationAct extends Activity implements IDialogSupportedActivity
{
    private ImageButton buttonInputAdminName;
    private EditText inputAdminName;

    private Context context;

    private SharedPreferences props;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.authorization_act);

        context = this;
        DataBaseManager.init(context);

        setUpControl();
    }


    private void setUpControl()
    {

        buttonInputAdminName = (ImageButton) findViewById(R.id.buttonGo);
        buttonInputAdminName.setOnClickListener(onClickButtonInputAdminName);

        inputAdminName = (EditText) findViewById(R.id.inputAdminName);

        props = context.getSharedPreferences(App.PROPERTIES_FILE_NAME, Context.MODE_PRIVATE);

        if (props.contains(App.PROPERTY_ADMIN_NAME_APP))
        {
            inputAdminName.setText(props.getString(App.PROPERTY_ADMIN_NAME_APP, ""));
        }
        else
        {
            AccountManager accountManager = (AccountManager) getSystemService(ACCOUNT_SERVICE);
            //Account[] list = manager.getAccounts();
            Account[] accounts = accountManager.getAccountsByType("com.google");
            inputAdminName.setText(accounts[0].name);
//            String email="";
//            email=accountManager.getUserData(accounts[0], accountManager.KEY_USERDATA);
        }

    }


    private View.OnClickListener onClickButtonInputAdminName = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            if (!String.valueOf(inputAdminName.getText()).isEmpty())
            {

                SharedPreferences.Editor editor = context.getSharedPreferences(App.PROPERTIES_FILE_NAME, Context.MODE_PRIVATE).edit();

                if (props.contains(App.PROPERTY_ADMIN_ID))//если айдишник есть
                {
                    try
                    {
                        //Обновляем данные в базе
                        UserContact userContact = DataBaseManager.getInstance().getUserContactByIndexContact("0");
                        userContact.setUserName(String.valueOf(inputAdminName.getText()));
                        DataBaseManager.getInstance().createOrUpdateUserContacts(userContact);

                    }
                    catch (SQLException e)
                    {
                        e.printStackTrace();
                    }
                }
                else
                {//если нету, то создаём новый контакт с главным админом

                    UserContact userContact = new UserContact("0", String.valueOf(inputAdminName.getText()), "654654654", null);

                    try
                    {
                        DataBaseManager.getInstance().createUserContacts(userContact);

                        editor.putString(App.PROPERTY_ADMIN_ID, String.valueOf(DataBaseManager.getInstance().getUserContactByIndexContact("0").getId()));//Запоминаем айдишник админа
                    }
                    catch (SQLException e)
                    {
                        e.printStackTrace();
                    }
                }

                editor.putString(App.PROPERTY_ADMIN_NAME_APP, String.valueOf(inputAdminName.getText()));

                editor.commit();

                ActivityLauncher.showMainAct(context);
            }
            else
            {
                showDialog(IDialogSupportedActivity.WARNING_DIALOG,
                        getString(R.string.AuthorizationAct_InputAdminNameDialogTitle),
                        getString(R.string.AuthorizationAct_InputAdminNameDialogDescription),
                        null, null);
            }
        }
    };


    @Override
    public void showDialog(String dialogType, String title, String message, ButtonDialog actionPositive, ButtonDialog actionNegative)
    {

    }
}
