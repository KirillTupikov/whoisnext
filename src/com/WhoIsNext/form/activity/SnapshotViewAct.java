package com.WhoIsNext.form.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.WhoIsNext.WhoIsNext.R;
import com.WhoIsNext.data.Lunch;
import com.WhoIsNext.managers.ImageManager;

/**
 * Created by kirill.tupikov on 10/2/13.
 */
public class SnapshotViewAct extends Activity
{

    private ImageView imageCheck;
    private Button buttonReady;
    private Button buttonDelete;
    private String ImagePath;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.snapshot_view_act);

        Intent intent = getIntent();
        ImagePath = (String) intent.getStringExtra(Lunch.IMAGE_CHECK_FIELD_NAME);

        setUpControl();
    }

    private void setUpControl()
    {
        imageCheck = (ImageView) findViewById(R.id.imageCheck);

        imageCheck.setImageBitmap(BitmapFactory.decodeFile(ImageManager.getInstance().getDirectory().getPath() + "/" + ImagePath));

        buttonReady = (Button) findViewById(R.id.buttonReady);
        buttonReady.setOnClickListener(onClickReady);

        buttonDelete = (Button) findViewById(R.id.buttonDelete);
        buttonDelete.setOnClickListener(onClickDelete);
    }

    private View.OnClickListener onClickReady = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            finish();
        }
    };

    private View.OnClickListener onClickDelete = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            if (ImageManager.getInstance().deleteImage(ImagePath))
            {
                Intent intent = new Intent();
                intent.putExtra(Lunch.IMAGE_CHECK_FIELD_NAME, ImagePath);
                setResult(RESULT_OK, intent);
                finish();
            }
        }
    };
}
