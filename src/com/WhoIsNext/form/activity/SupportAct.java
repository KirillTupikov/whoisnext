package com.WhoIsNext.form.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.InputFilter;
import android.text.InputType;
import android.text.method.DigitsKeyListener;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.WhoIsNext.App;
import com.WhoIsNext.Settings;
import com.WhoIsNext.WhoIsNext.R;
import com.WhoIsNext.form.IDialogSupportedActivity;
import com.WhoIsNext.form.dialog.ButtonDialog;
import com.WhoIsNext.managers.APIManager;

import java.util.regex.Pattern;

/**
 * Created by Helm on 03.10.13.
 */
public class SupportAct extends BaseActivity
{
    private EditText txtName;
    private EditText txtPhone;
    private EditText txtEmail;
    private EditText complaintText;
    private LinearLayout frameButtons;
    private boolean sending = false;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.support_act);

        setTitle(R.string.SupportAct_SupportTitle);

        txtName = (EditText) findViewById(R.id.txtName);
        txtPhone = (EditText) findViewById(R.id.txtPhone);
        txtEmail = (EditText) findViewById(R.id.txtEmail);

        complaintText = (EditText) findViewById(R.id.complaintText);
        complaintText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        complaintText.setSingleLine(true);
        complaintText.setLines(5);// desired number of lines
        complaintText.setHorizontallyScrolling(false);
        complaintText.setImeOptions(EditorInfo.IME_ACTION_DONE);

        int cardLength = Settings.COUNT_OF_PHONE_NUMBERS;
        txtPhone.setFilters(new InputFilter[]{new InputFilter.LengthFilter(cardLength), new DigitsKeyListener()});

        frameButtons = (LinearLayout) findViewById(R.id.frameButtonSupport);

        Resources res = getResources();
        Button btnSend = new Button(this);
        btnSend.setBackgroundDrawable(res.getDrawable(R.drawable.btn));
        btnSend.setGravity(Gravity.CENTER);
        //btnSend.setTextAppearance(context, R.style.button_text_style);
        btnSend.setText(getString(R.string.send_message));
        btnSend.setOnClickListener(onSendClickListener);

        frameButtons.addView(btnSend);

        SharedPreferences props = context.getSharedPreferences(App.PROPERTIES_FILE_NAME, Context.MODE_PRIVATE);
//        props = App.getInstance().getProperties();

        if (props.contains(App.PROPERTY_ADMIN_NAME_APP))
        {
            txtName.setText(props.getString(App.PROPERTY_ADMIN_NAME_APP, ""));
        }

        try
        {
            TelephonyManager telephonyManager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
            if (!telephonyManager.getLine1Number().isEmpty())
            {
                String number = telephonyManager.getLine1Number();
                if (number.length() >= 10)
                {
                    number = number.substring(number.length() - 10, number.length());
                }
                txtPhone.setText(number);
            }
            else
            {
                lastPhone();
            }
        } catch (Exception e)
        {
            lastPhone();
        }

        if (props.contains(App.PROPERTY_USER_PHONE))
        {
            String userPhone = props.getString(App.PROPERTY_USER_PHONE, "");
            if (userPhone.length() > 0)
            {
                txtPhone.setText(userPhone);
            }
        }

        if (props.contains(App.PROPERTY_USER_EMAIL))
        {
            txtEmail.setText(props.getString(App.PROPERTY_USER_EMAIL, ""));
        }
    }

    private void lastPhone()
    {
//
        SharedPreferences preferences = context.getSharedPreferences(App.PROPERTIES_FILE_NAME, Context.MODE_PRIVATE);
        txtPhone.setText(preferences.getString(App.PROPERTY_USER_PHONE, ""));
    }


    private View.OnClickListener onSendClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            if (checkNetworkStatus(false))
                if (checkPost())
                {
                    //Убираем клавиатуру
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(txtName.getApplicationWindowToken(), 0);
                    //
                    SendTask sendTask = new SendTask();
                    sendTask.execute();
                }
        }
    };


    private class SendTask extends AsyncTask<String, Void, String>
    {
        protected String doInBackground(String... params)
        {

            if (!sending)
            {

                String text = complaintText.getText().toString();
                String name = txtName.getText().toString();
                String phone = txtPhone.getText().toString();
                String email = txtEmail.getText().toString();

                // LogManager.addRecord("Send complaint");
                sending = true;
                SharedPreferences.Editor editor = context.getSharedPreferences(App.PROPERTIES_FILE_NAME, Context.MODE_PRIVATE).edit();
                editor.putString(App.PROPERTY_USER_NAME, name);
                editor.putString(App.PROPERTY_USER_PHONE, phone);
                editor.putString(App.PROPERTY_USER_EMAIL, email);
                editor.commit();

                APIManager.sendComplaint(name, phone, email, text, context);

                showDialog(INFO_DIALOG, context.getString(R.string.support),
                        context.getString(R.string.message_sent),
                        new ButtonDialog(
                                new Runnable()
                                {
                                    @Override
                                    public void run()
                                    {
                                        complaintText.setText("");
                                    }
                                }, getString(R.string.continue_str)),
                        null);


                sending = false;
            }
            return null;
        }

        @Override
        protected void onPreExecute()
        {
            showDialog(WAITING_DIALOG, getString(R.string.support), getString(R.string.message_sending), null,
                    new ButtonDialog(
                            new Runnable()
                            {
                                @Override
                                public void run()
                                {

                                }
                            }, getString(R.string.cancel))
            );
        }

        @Override
        protected void onPostExecute(String result)
        {
            //stopDialog();
            //waitDialog.dismiss();
        }
    }

    public boolean checkEmail(String email)
    {
        Pattern pattern = Pattern.compile("^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\\.([a-zA-Z])+([a-zA-Z])+");
        return pattern.matcher(email).matches();
    }

    @Override
    protected void onPause()
    {
        super.onPause();    //To change body of overridden methods use File | Settings | File Templates.
        if (!txtPhone.getText().toString().isEmpty())
        {
            SharedPreferences.Editor editor = context.getSharedPreferences(App.PROPERTIES_FILE_NAME, Context.MODE_PRIVATE).edit();
            editor.putString(App.PROPERTY_USER_PHONE, txtPhone.getText().toString());
            editor.commit();
        }
    }

    private boolean checkPost()
    {
        String text = complaintText.getText().toString();
        String name = txtName.getText().toString();
        String phone = txtPhone.getText().toString();
        String email = txtEmail.getText().toString();

        if (name.equals(""))
        {
            showDialog(WARNING_DIALOG, context.getString(R.string.support),
                    context.getString(R.string.empty_name),
                    new ButtonDialog(
                            new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    txtName.requestFocus();
                                }
                            }, getString(R.string.continue_str)),
                    null);

            return false;
        }

        if ((phone.equals("")) && (email.equals("")))
        {
            showDialog(WARNING_DIALOG, context.getString(R.string.support),
                    context.getString(R.string.empty_phone_email),
                    new ButtonDialog(
                            new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    txtPhone.requestFocus();
                                }
                            }, getString(R.string.continue_str)),
                    null);

            return false;
        }

        if (!checkEmail(email) && !email.equals(""))
        {
            showDialog(WARNING_DIALOG, context.getString(R.string.support),
                    context.getString(R.string.email_is_not_valid),
                    new ButtonDialog(
                            new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    txtEmail.requestFocus();
                                }
                            }, getString(R.string.continue_str)),
                    null);

            return false;
        }


        if (text.equals(""))
        {
            showDialog(WARNING_DIALOG, context.getString(R.string.support),
                    context.getString(R.string.empty_message),
                    new ButtonDialog(
                            new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    complaintText.requestFocus();
                                }
                            }, getString(R.string.continue_str)),
                    null);

            return false;
        }
        return true;
    }
}
