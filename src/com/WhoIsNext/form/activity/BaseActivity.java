package com.WhoIsNext.form.activity;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;

import com.WhoIsNext.ActivityLauncher;
import com.WhoIsNext.App;
import com.WhoIsNext.WhoIsNext.R;
import com.WhoIsNext.form.IDialogSupportedActivity;
import com.WhoIsNext.form.dialog.AppModalDialog;
import com.WhoIsNext.form.dialog.ButtonDialog;

/**
 * Created by kirill.tupikov on 7/9/13.
 */
public class BaseActivity extends Activity implements IDialogSupportedActivity
{
    protected Context context;
    protected ActionBar actionBar;
    protected final String TAG = getClass().getSimpleName();//?
    protected Handler handler = new Handler();
    private AppModalDialog modalDialog;
    private Class<?> previousActivity;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        context = this;

        actionBar = getActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayUseLogoEnabled(false);
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#ffba33")));
        actionBar.setIcon(R.drawable.ic_header);
        actionBar.setDisplayHomeAsUpEnabled(true);

        setPreviousActivity(MainAct.class);//по дефолту- это главная форма
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.main_menu, menu);

        MenuItem mi = menu.add(0, 1, 0, getString(R.string.menu_setting));
        mi.setIntent(new Intent(this, PreferenceApplication.class));
        return super.onCreateOptionsMenu(menu);
    }

    protected void setPreviousActivity(Class<?> previousActivity)
    {
        this.previousActivity = previousActivity;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
//                Intent intent = new Intent(this, previousActivity);
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                startActivity(intent);
                finish();
                return true;

            case R.id.menuProfile:

                SharedPreferences props = context.getSharedPreferences(App.PROPERTIES_FILE_NAME, Context.MODE_PRIVATE);

                if (props.contains(App.PROPERTY_ADMIN_ID))
                {
                    ActivityLauncher.showPersonalProfileAct(context, props.getString(App.PROPERTY_ADMIN_ID, ""));
                }
                break;
            case R.id.menuAboutProgram:

                ActivityLauncher.showAboutProgramAct(context);
                break;
            case R.id.menuSupport:

                ActivityLauncher.showSupportAct(context);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return super.onOptionsItemSelected(item);
    }

    public void stopDialog()
    {
        if (modalDialog != null)
        {
            modalDialog.dismissDialog();
        }
    }

    @Override
    public void showDialog(String dialogType, String title, String message, ButtonDialog actionPositive, ButtonDialog actionNegative)
    {
        if (modalDialog != null)
        {
            modalDialog.stop();
        }

        if (actionPositive != null && actionNegative != null)
        {
            modalDialog = new AppModalDialog(handler, this, dialogType, title, message, actionPositive, actionNegative);
        }
        else
            if (actionPositive != null)
            {
                modalDialog = new AppModalDialog(handler, this, dialogType, title, message, actionPositive);
            }
            else
                if (actionNegative != null)
                {
                    modalDialog = new AppModalDialog(handler, this, dialogType, title, message, null, actionNegative);
                }
                else
                    if (actionPositive == null)
                    {
                        modalDialog = new AppModalDialog(handler, this, dialogType, title, message);
                    }
        runOnUiThread(modalDialog);
    }

    public void setTitleActionBar(String titleActionBar)
    {
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(titleActionBar);
    }

    public boolean checkNetworkStatus(final boolean closeView)
    {
        try
        {
            final ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo nInfo = connMgr.getActiveNetworkInfo();

            final android.net.NetworkInfo wifi = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            final android.net.NetworkInfo mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            if (nInfo != null)
            {
                if ((wifi.isAvailable() || mobile.isAvailable()) && nInfo.isConnected())
                {
                    //if(connMgr.requestRouteToHost(ConnectivityManager.TYPE_WIFI, ) )

                    //if (APIManager.checkConnection())
                    return true;
                }
            }

            showDialog(WARNING_DIALOG, getString(R.string.connection), getString(R.string.problem_with_ethernet),
                    new ButtonDialog(
                            new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    if (closeView)
                                        finish();
                                }
                            }, getString(R.string.continue_str))
                    , null);

            return false;
        }
        catch (NullPointerException ex)
        {
            return false;
        }
    }


//    protected void setMenuBackground()
//    {
//        getLayoutInflater().setFactory(new LayoutInflater.Factory()
//        {
//            public View onCreateView(String name, Context context, AttributeSet attrs)
//            {
//                if (name.equalsIgnoreCase("com.android.internal.view.menu.IconMenuItemView"))
//                {
//                    try
//                    { // Ask our inflater to create the view
//                        LayoutInflater f = getLayoutInflater();
//                        final View view = f.createView(name, null, attrs);
//                        /* The background gets refreshed each time a new item is added the options menu.
//                        * So each time Android applies the default background we need to set our own
//                        * background. This is done using a thread giving the background change as runnable
//                        * object */
//                        new Handler().post(new Runnable()
//                        {
//                            public void run()
//                            {
//                                // sets the background color
//                                view.setBackgroundResource(R.color.white);
//                                // sets the text color
//                                ((TextView) view).setTextColor(Color.BLACK);
//                                // sets the text size
//                                ((TextView) view).setTextSize(18);
//                            }
//                        });
//                        return view;
//                    }
//                    catch (InflateException e)
//                    {
//                    }
//                    catch (ClassNotFoundException e)
//                    {
//                    }
//                }
//                return null;
//            }
//        });
//    }

}
