package com.WhoIsNext.form.dialog.impl;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import com.WhoIsNext.WhoIsNext.R;
import com.WhoIsNext.form.dialog.ButtonDialog;


/**
 * Created with IntelliJ IDEA.
 * User: Kirill
 * Date: 26.04.13
 * Time: 18:37
 * To change this template use File | Settings | File Templates.
 */
public class InfoModalDialog extends AlertModalDialog
{

    public InfoModalDialog(Context context, String title, String message, final ButtonDialog actionPositive, final ButtonDialog actionNegative)
    {
        super(context, title, message, actionPositive, actionNegative);

        String buttonTitle;

        if (actionPositive == null)
            buttonTitle = context.getString(R.string.continue_str);
        else
            buttonTitle = actionPositive.getTitle();

        this.setButton(InfoModalDialog.BUTTON_POSITIVE, buttonTitle, new OnClickListener()
        {
            public void onClick(DialogInterface dialog, int which)
            {
                runUserActionPositive();
            }

        });

        if (actionNegative != null)
            this.setButton(InfoModalDialog.BUTTON_NEGATIVE, actionNegative.getTitle(), new OnClickListener()
            {
                public void onClick(DialogInterface dialog, int which)
                {
                    runUserActionNegative();
                }

            });
    }

    @Override
    protected Drawable getDialogIcon()
    {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }
}