package com.WhoIsNext.form.dialog;

import android.app.Activity;
import android.os.Handler;
import com.WhoIsNext.form.IDialogSupportedActivity;
import com.WhoIsNext.form.dialog.impl.*;


/**
 * Created with IntelliJ IDEA.
 * User: Kirill
 * Date: 26.04.13
 * Time: 17:32
 * To change this template use File | Settings | File Templates.
 */
public class AppModalDialog implements Runnable
{

    protected Handler handler;
    protected Activity activity;
    protected String dialogType;
    protected String title;
    protected String message;
    protected ButtonDialog actionPositive;
    protected ButtonDialog actionNegative;

    private IModalDialog openedDialog;

    public AppModalDialog(Handler handler, Activity activity, String dialogType, String title, String message)
    {
        this(handler, activity, dialogType, title, message, null, null);
    }

    public AppModalDialog(Handler handler, Activity activity, String dialogType, String title, String message, ButtonDialog actionPositive)
    {
        this(handler, activity, dialogType, title, message, actionPositive, null);
    }

    public AppModalDialog(Handler handler, Activity activity, String dialogType, String title, String message, ButtonDialog actionPositive, ButtonDialog actionNegative)
    {
        this.handler = handler;
        this.activity = activity;
        this.dialogType = dialogType;
        this.title = title;
        this.message = message;
        this.actionPositive = actionPositive;
        this.actionNegative = actionNegative;
    }

    @Override
    public void run()
    {
        showDialog();
    }

    public void stop()
    {
        if (openedDialog != null)
        {
            try
            {
                openedDialog.close();
            }
            catch (Exception ex)
            {
                // do nothing
            }
        }
    }

    public void dismissDialog()
    {
        if (openedDialog != null)
        {
            try
            {
                openedDialog.dismiss();
            }
            catch (Exception ex)
            {
                // do nothing
            }
        }
    }


    public void showDialog()
    {

        if (IDialogSupportedActivity.ERROR_DIALOG.equals(dialogType))
        {
            openedDialog = new ErrorModalDialog(activity, title, message, actionPositive, actionNegative);
        }
        if (IDialogSupportedActivity.WARNING_DIALOG.equals(dialogType))
        {
            openedDialog = new WarningModalDialog(activity, title, message, actionPositive, actionNegative);
        }
        if (IDialogSupportedActivity.INFO_DIALOG.equals(dialogType))
        {
            openedDialog = new InfoModalDialog(activity, title, message, actionPositive, actionNegative);
        }
        if (IDialogSupportedActivity.WAITING_DIALOG.equals(dialogType))
        {
            openedDialog = new ProgressModalDialog(activity, title, message, actionPositive, actionNegative);
        }

        if (openedDialog == null)
        {
            throw new IllegalArgumentException("Dialog type isn't supported: " + dialogType);
        }

        handler.post(new ModalDialogProcess(activity, openedDialog));

    }

    private class ModalDialogProcess implements Runnable
    {

        protected Activity activity;
        private IModalDialog dialogToShow;

        private ModalDialogProcess(Activity activity, IModalDialog dialogToShow)
        {
            this.activity = activity;
            this.dialogToShow = dialogToShow;
        }

        @Override
        public void run()
        {
            if (!activity.isFinishing())
            {
                dialogToShow.show();
            }
        }
    }

}
