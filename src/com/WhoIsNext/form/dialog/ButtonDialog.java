package com.WhoIsNext.form.dialog;

/**
 * Created with IntelliJ IDEA.
 * User: Helm
 * Date: 29.04.13
 * Time: 12:46
 * To change this template use File | Settings | File Templates.
 */
public class ButtonDialog
{
    private Runnable action;
    private String title;

    public ButtonDialog(Runnable action, String title)
    {
        this.action = action;
        this.title = title;
    }

    public Runnable getAction()
    {
        return action;
    }

    public void setAction(Runnable action)
    {
        this.action = action;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }
}
