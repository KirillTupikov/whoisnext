package com.WhoIsNext;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.ViewConfiguration;

import java.lang.reflect.Field;

/**
 * Created by kirill.tupikov on 7/8/13.
 */
public class App extends Application
{
    private static App instance = null;

    public static final String PROPERTIES_FILE_NAME = "properties";
    public static final String PROPERTY_USER_NAME = "user_name";
    public static final String PROPERTY_USER_PHONE = "user_phone";
    public static final String PROPERTY_USER_EMAIL = "user_email";
    public static final String PROPERTY_CURRENCY = "currency";
    public static final String PROPERTY_MAX_SUM = "max_sum";
    public static final String PROPERTY_ADMIN_ID = "admin_id";
    public static final String PROPERTY_ADMIN_NAME_APP = "admin_app";
    public static final String PROPERTY_ADMIN_APP_AVATAR = "admin_app_avatar";
    private int maximumSizeImage = 0;
    //public ArrayList<String> currentImageCheck = new ArrayList<String>();

    @Override
    public void onCreate()
    {
        super.onCreate();
        getOverflowMenu();
    }

    public static App getInstance()
    {
        if (instance == null)
        {
            instance = new App();
        }
        return instance;
    }

    private void getOverflowMenu()
    {

        try
        {
            ViewConfiguration config = ViewConfiguration.get(this);
            Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
            if (menuKeyField != null)
            {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public static SharedPreferences getPreferences()
    {
        return instance.getSharedPreferences(PROPERTIES_FILE_NAME, 0);
    }

    public static String getCurrency(Context context)
    {
        SharedPreferences props = context.getSharedPreferences(App.PROPERTIES_FILE_NAME, Context.MODE_PRIVATE);
        String currency = props.getString(App.PROPERTY_CURRENCY, "currency_bel");

        if (currency.equals(Settings.CURRENCY_BEL))
            return " т.";
        else
            if (currency.equals(Settings.CURRENCY_RUS))
                return " руб.";
            else
                if (currency.equals(Settings.CURRENCY_USD))
                    return " $";
                else
                    if (currency.equals(Settings.CURRENCY_EUR))
                        return " e.";
        return " т.";
    }

    public int getMaximumSizeImage()
    {
        return maximumSizeImage;
    }

    public void setMaximumSizeImage(int maximumSizeImage)
    {
        this.maximumSizeImage = maximumSizeImage;
    }
}
