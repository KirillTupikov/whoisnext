package com.WhoIsNext.gui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.WhoIsNext.App;
import com.WhoIsNext.WhoIsNext.R;
import com.WhoIsNext.data.User;
import com.WhoIsNext.managers.ImageManager;

import java.util.ArrayList;

/**
 * Created by kirill.tupikov on 7/9/13.
 */
public class LunchListAdapter extends ArrayAdapter<User>
{
    private ArrayList<User> contentItems;
    private LayoutInflater layoutInflater;
    private Context context;

    private static class ViewHolder
    {
        ImageView iconUser;
        TextView userName;
        TextView userSum;
        ImageView userStatus;
    }

    public LunchListAdapter(Context context, int textViewResourceId, ArrayList<User> contentItems)
    {
        super(context, textViewResourceId, contentItems);
        this.context = context;
        this.contentItems = contentItems;

        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount()
    {
        return contentItems.size();
    }


//    @Override
//    public Object getItem(int i)
//    {
//        return contentItems.get(i);
//    }

    @Override
    public long getItemId(int i)
    {
        // return Long.parseLong(contentItems.get(i).getUserId());
        return (long) i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup)
    {
        ViewHolder viewHolder;

        if (view == null)
        {
            view = layoutInflater.inflate(R.layout.item_lunch, null);
            viewHolder = new ViewHolder();

            viewHolder.iconUser = (ImageView) view.findViewById(R.id.iconUser);
            viewHolder.userName = (TextView) view.findViewById(R.id.userName);
            viewHolder.userSum = (TextView) view.findViewById(R.id.userSum);
            viewHolder.userStatus = (ImageView) view.findViewById(R.id.userStatus);

            view.setTag(viewHolder);
        }
        else
        {
            viewHolder = (ViewHolder) view.getTag();
        }

        User userItem = (User) getItem(i);

        //Bitmap phPhoto = ImageManager.getInstance().loadContactPhoto(context.getContentResolver(), Long.valueOf(userItem.getUserContact().getUserContactId()));
        Bitmap phPhoto = ImageManager.getInstance().loadImageByPath(userItem.getUserContact().getUserPhoto());
        if (phPhoto == null)
            viewHolder.iconUser.setImageBitmap(ImageManager.getInstance().circleAvatar(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_person)));
        else
            viewHolder.iconUser.setImageBitmap(ImageManager.getInstance().circleAvatar(phPhoto));

        viewHolder.userName.setText(userItem.getUserContact().getUserName().toString());

        viewHolder.userSum.setText(String.valueOf(userItem.getDebtSum()) + App.getCurrency(context));

        if (User.USER_STATUS_PAY.equals(userItem.getStatus()))
        {
            viewHolder.userStatus.setImageResource(R.drawable.ic_clock);
        }
        else
            if (User.USER_STATUS_NOT_PAY.equals(userItem.getStatus()))
            {
                viewHolder.userStatus.setImageResource(R.drawable.ic_aim);
            }
            else
                if (User.USER_STATUS_DISABLED.equals(userItem.getStatus()))
                {
                    viewHolder.userStatus.setImageResource(R.drawable.ic_remove);
                }
                else
                {
                    if (User.USER_STATUS_ADMIN_LUNCH.equals(userItem.getStatus()))
                    {
                        viewHolder.userStatus.setImageResource(R.drawable.ic_coins);
                    }
                    else
                    {
                        viewHolder.userStatus.setImageResource(R.drawable.ic_aim);
                    }
                }


        return view;
    }
}
