package com.WhoIsNext.gui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.WhoIsNext.ActivityLauncher;
import com.WhoIsNext.WhoIsNext.R;
import com.WhoIsNext.data.Group;
import com.WhoIsNext.data.UserGroup;
import com.WhoIsNext.managers.ImageManager;

import java.util.ArrayList;

/**
 * Created by kirill.tupikov on 7/8/13.
 */
public class PartyListAdapter extends BaseAdapter
{
    private ArrayList<Group> contentItems;
    private LayoutInflater layoutInflater;
    private Context context;

    private static class ViewHolder
    {
        ImageView iconParty;
        TextView nameParty;
        ImageView statusParty;

    }

    public PartyListAdapter(Context context, ArrayList<Group> contentItems)
    {
        this.context = context;
        this.contentItems = contentItems;

        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount()
    {
        return contentItems.size();
    }

    @Override
    public Object getItem(int i)
    {
        return contentItems.get(i);
    }

    @Override
    public long getItemId(int i)
    {
        return (long) i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup)
    {
        ViewHolder viewHolder;

        if (view == null)
        {
            view = layoutInflater.inflate(R.layout.item_party, null);
            viewHolder = new ViewHolder();

            viewHolder.iconParty = (ImageView) view.findViewById(R.id.iconParty);
            viewHolder.nameParty = (TextView) view.findViewById(R.id.nameParty);
            viewHolder.statusParty = (ImageView) view.findViewById(R.id.statusParty);

            view.setTag(viewHolder);
        }
        else
        {
            viewHolder = (ViewHolder) view.getTag();
        }

        final Group partyItem = (Group) getItem(i);

        Bitmap phPhoto = ImageManager.getInstance().loadImageByPath(partyItem.getGroupPhoto());
        if(phPhoto == null)
            viewHolder.iconParty.setImageBitmap(ImageManager.getInstance().circleAvatar(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_group)));
        else
            viewHolder.iconParty.setImageBitmap(ImageManager.getInstance().circleAvatar(phPhoto));
        //viewHolder.iconParty.setImageBitmap(ImageManager.getInstance().circleAvatar(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_group)));

        //viewHolder.iconParty.setImageResource(R.drawable.ic_group);
        viewHolder.nameParty.setText(partyItem.getGroupName());

        if (partyItem.getStatus().equals(Group.STATUS_PARTY_ENABLED))
            viewHolder.statusParty.setImageResource(R.drawable.ic_aim);
        else
            viewHolder.statusParty.setImageResource(R.drawable.ic_clock);

        viewHolder.statusParty.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                ActivityLauncher.showHistoryLunchesAct(context, partyItem.getId());
            }
        });

        return view;
    }
}
