package com.WhoIsNext.gui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.WhoIsNext.App;
import com.WhoIsNext.WhoIsNext.R;
import com.WhoIsNext.data.User;
import com.WhoIsNext.data.UserLunch;
import com.WhoIsNext.managers.ImageManager;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: kirill.tupikov
 * Date: 9/24/13
 * Time: 5:22 PM
 * To change this template use File | Settings | File Templates.
 */
public class MemberOfLunchAdapter extends BaseAdapter
{
    private ArrayList<UserLunch> contentItems;
    private LayoutInflater layoutInflater;
    private Context context;

    private static class ViewHolder
    {
        ImageView iconUser;
        TextView userName;
        TextView userSum;
    }

    public MemberOfLunchAdapter(Context context, ArrayList<UserLunch> contentItems)
    {
        this.context = context;
        this.contentItems = contentItems;

        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount()
    {
        return contentItems.size();
    }

    @Override
    public Object getItem(int i)
    {
        return contentItems.get(i);
    }

    @Override
    public long getItemId(int i)
    {
        return (long) i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup)
    {
        ViewHolder viewHolder;

        if (view == null)
        {
            view = layoutInflater.inflate(R.layout.item_members_of_lunch, null);
            viewHolder = new ViewHolder();

            viewHolder.iconUser = (ImageView) view.findViewById(R.id.iconUser);
            viewHolder.userName = (TextView) view.findViewById(R.id.userName);
            viewHolder.userSum = (TextView) view.findViewById(R.id.userSum);

            view.setTag(viewHolder);
        }
        else
        {
            viewHolder = (ViewHolder) view.getTag();
        }

        UserLunch userLunchItem = (UserLunch) getItem(i);

        //Bitmap phPhoto = ImageManager.getInstance().loadContactPhoto(context.getContentResolver(), Long.valueOf(userLunchItem.getUser().getUserContact().getUserContactId()));
        Bitmap phPhoto = ImageManager.getInstance().loadImageByPath(userLunchItem.getUser().getUserContact().getUserPhoto());
        if(phPhoto == null)
            viewHolder.iconUser.setImageBitmap(ImageManager.getInstance().circleAvatar(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_person)));
        else
            viewHolder.iconUser.setImageBitmap(ImageManager.getInstance().circleAvatar(phPhoto));

        viewHolder.userName.setText(userLunchItem.getUser().getUserContact().getUserName().toString());
        viewHolder.userSum.setText(String.valueOf(userLunchItem.getPaymentSum()) + App.getCurrency(context));


        return view;
    }
}
