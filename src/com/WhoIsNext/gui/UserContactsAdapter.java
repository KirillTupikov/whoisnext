package com.WhoIsNext.gui;

import android.app.Activity;
import android.app.Application;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.WhoIsNext.App;
import com.WhoIsNext.WhoIsNext.R;
import com.WhoIsNext.data.Group;
import com.WhoIsNext.data.UserContact;
import com.WhoIsNext.managers.ImageManager;

import java.io.InputStream;
import java.util.ArrayList;

import static android.provider.Contacts.People.loadContactPhoto;

/**
 * Created with IntelliJ IDEA.
 * User: kirill.tupikov
 * Date: 9/27/13
 * Time: 5:01 PM
 * To change this template use File | Settings | File Templates.
 */
public class UserContactsAdapter extends ArrayAdapter<UserContact>
{
    private ArrayList<UserContact> contentItems;
    private LayoutInflater layoutInflater;
    private Context context;

    private static class ViewHolder
    {
        ImageView iconUser;
        TextView nameUser;
        ImageView addUserToGroup;

    }

    public UserContactsAdapter(Context context, int textViewResourceId, ArrayList<UserContact> contentItems)
    {
        super(context, textViewResourceId, contentItems);
        this.context = context;
        this.contentItems = contentItems;

        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup)
    {
        ViewHolder viewHolder;

        if (view == null)
        {
            view = layoutInflater.inflate(R.layout.item_user_contacts, null);
            viewHolder = new ViewHolder();

            viewHolder.iconUser = (ImageView) view.findViewById(R.id.iconUserContacts);
            viewHolder.nameUser = (TextView) view.findViewById(R.id.nameUserContacts);
            viewHolder.addUserToGroup = (ImageView) view.findViewById(R.id.addUserToGroup);

            view.setTag(viewHolder);
        }
        else
        {
            viewHolder = (ViewHolder) view.getTag();
        }

        UserContact userContactItem = (UserContact) getItem(i);

        Bitmap phPhoto = ImageManager.getInstance().loadContactPhoto(context.getContentResolver(), Long.valueOf(userContactItem.getUserContactId()));
        //Bitmap phPhoto = ImageManager.getInstance().loadImageByPath(userContactItem.getUserPhoto());
        if (phPhoto == null)
            viewHolder.iconUser.setImageBitmap(ImageManager.getInstance().circleAvatar(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_person)));
        else
            viewHolder.iconUser.setImageBitmap(ImageManager.getInstance().circleAvatar(phPhoto));

        viewHolder.nameUser.setText(userContactItem.getUserName());

        if (userContactItem.isAddedToGroup())
        {
            viewHolder.addUserToGroup.setImageResource(R.drawable.ic_action_accept);
            //view.setBackgroundColor(context.getResources().getColor(R.color.added_user_contact));
        }
        else
            viewHolder.addUserToGroup.setImageResource(R.drawable.ic_action_empty);

//        if (userContactItem.getStatus().equals(Group.STATUS_PARTY_ENABLED))
//            viewHolder.statusParty.setImageResource(R.drawable.ic_clock);
//        else
//            viewHolder.statusParty.setImageResource(R.drawable.ic_aim);

        return view;
    }
}
