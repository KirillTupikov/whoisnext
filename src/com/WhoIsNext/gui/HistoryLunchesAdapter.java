package com.WhoIsNext.gui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.WhoIsNext.App;
import com.WhoIsNext.WhoIsNext.R;
import com.WhoIsNext.data.Lunch;
import com.WhoIsNext.data.User;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created with IntelliJ IDEA.
 * User: kirill.tupikov
 * Date: 9/23/13
 * Time: 3:02 PM
 * To change this template use File | Settings | File Templates.
 */
public class HistoryLunchesAdapter extends BaseAdapter
{
    private ArrayList<Lunch> contentItems;
    private LayoutInflater layoutInflater;
    private Context context;

    private static class ViewHolder
    {
        TextView lunchDate;
        TextView lunchSum;
        TextView countUsers;
    }

    public HistoryLunchesAdapter(Context context, ArrayList<Lunch> contentItems)
    {
        this.context = context;
        this.contentItems = contentItems;

        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount()
    {
        return contentItems.size();
    }

    @Override
    public Object getItem(int i)
    {
        return contentItems.get(i);
    }

    @Override
    public long getItemId(int i)
    {
        return (long) i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup)
    {
        ViewHolder viewHolder;

        if (view == null)
        {
            view = layoutInflater.inflate(R.layout.item_lunches_history, null);
            viewHolder = new ViewHolder();

            viewHolder.lunchDate = (TextView) view.findViewById(R.id.lunchDate);
            viewHolder.lunchSum = (TextView) view.findViewById(R.id.lunchSum);
            viewHolder.countUsers = (TextView) view.findViewById(R.id.countUsers);

            view.setTag(viewHolder);
        }
        else
        {
            viewHolder = (ViewHolder) view.getTag();
        }

        Lunch lunchItem = (Lunch) getItem(i);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM-dd-yyyy HH:mm");
        String date = simpleDateFormat.format(lunchItem.getDate());

        viewHolder.lunchDate.setText(date);
        viewHolder.lunchSum.setText(String.valueOf(lunchItem.getSumLunch() + App.getCurrency(context)));
        viewHolder.countUsers.setText(String.valueOf(lunchItem.getPaymentUserLunches().size()));

        return view;
    }
}
