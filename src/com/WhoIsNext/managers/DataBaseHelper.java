package com.WhoIsNext.managers;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.WhoIsNext.data.*;
import com.WhoIsNext.managers.dao.*;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

/**
 * Created by kirill.tupikov on 7/11/13.
 */
public class DataBaseHelper extends OrmLiteSqliteOpenHelper
{


    private static final String TAG = DataBaseHelper.class.getSimpleName();

    private static final String DATABASE_NAME = "WhoIsNext.sqlite";

    private static final int DATABASE_VERSION = 1;

    private UserDAO userDAO = null;
    private UserGroupDAO userGroupDAO = null;
    private LunchDAO lunchDAO = null;
    private UserLunchDAO userLunchDAO = null;
    private GroupDAO groupDAO = null;
    private UserContactDAO userContactDAO = null;

    public DataBaseHelper(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource)
    {
        try
        {
            TableUtils.createTable(connectionSource, UserGroup.class);
            TableUtils.createTable(connectionSource, User.class);
            TableUtils.createTable(connectionSource, Group.class);
            TableUtils.createTable(connectionSource, Lunch.class);
            TableUtils.createTable(connectionSource, UserLunch.class);
            TableUtils.createTable(connectionSource, UserContact.class);
        }
        catch (SQLException e)
        {
            Log.e(TAG, "error creating DB " + DATABASE_NAME);
            throw new RuntimeException(e);
        }
        catch (java.sql.SQLException e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVer, int newVer)
    {
        try
        {
            TableUtils.dropTable(connectionSource, UserGroup.class, true);
            TableUtils.dropTable(connectionSource, User.class, true);
            TableUtils.dropTable(connectionSource, Group.class, true);
            TableUtils.dropTable(connectionSource, Lunch.class, true);
            TableUtils.dropTable(connectionSource, UserLunch.class, true);
            TableUtils.dropTable(connectionSource, UserContact.class, true);
            onCreate(db, connectionSource);
        }
        catch (SQLException e)
        {
            Log.e(TAG, "error upgrading db " + DATABASE_NAME + "from ver " + oldVer);
            throw new RuntimeException(e);
        }
        catch (java.sql.SQLException e)
        {
            e.printStackTrace();
        }
    }

    public UserContactDAO getUserContactDAO() throws SQLException, java.sql.SQLException
    {
        if (userContactDAO == null)
        {
            userContactDAO = new UserContactDAO(getConnectionSource(), UserContact.class);
        }
        return userContactDAO;
    }

    public UserDAO getUserDAO() throws SQLException, java.sql.SQLException
    {
        if (userDAO == null)
        {
            userDAO = new UserDAO(getConnectionSource(), User.class);
        }
        return userDAO;
    }

    public UserGroupDAO getUserGroupDAO() throws SQLException, java.sql.SQLException
    {
        if (userGroupDAO == null)
        {
            userGroupDAO = new UserGroupDAO(getConnectionSource(), UserGroup.class);
        }
        return userGroupDAO;
    }

    public LunchDAO getLunchDAO() throws SQLException, java.sql.SQLException
    {
        if (lunchDAO == null)
        {
            lunchDAO = new LunchDAO(getConnectionSource(), Lunch.class);
        }
        return lunchDAO;
    }

    public UserLunchDAO getUserLunchDAO() throws SQLException, java.sql.SQLException
    {
        if (userLunchDAO == null)
        {
            userLunchDAO = new UserLunchDAO(getConnectionSource(), UserLunch.class);
        }
        return userLunchDAO;
    }

    public GroupDAO getGroupDAO() throws SQLException, java.sql.SQLException
    {
        if (groupDAO == null)
        {
            groupDAO = new GroupDAO(getConnectionSource(), Group.class);
        }
        return groupDAO;
    }

    @Override
    public void close()
    {
        super.close();
        userDAO = null;
        userGroupDAO = null;
        groupDAO = null;
        lunchDAO = null;
        userLunchDAO = null;
        userContactDAO = null;
    }

}
