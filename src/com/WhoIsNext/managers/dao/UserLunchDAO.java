package com.WhoIsNext.managers.dao;

import com.WhoIsNext.data.Group;
import com.WhoIsNext.data.Lunch;
import com.WhoIsNext.data.UserGroup;
import com.WhoIsNext.data.UserLunch;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: kirill.tupikov
 * Date: 8/6/13
 * Time: 5:22 PM
 * To change this template use File | Settings | File Templates.
 */
public class UserLunchDAO extends BaseDaoImpl<UserLunch, Integer>
{

    public UserLunchDAO(ConnectionSource connectionSource, Class<UserLunch> dataClass) throws SQLException
    {
        super(connectionSource, dataClass);
    }

    public ArrayList<UserLunch> getAllUserLunches() throws SQLException
    {
        return new ArrayList<UserLunch>(this.queryForAll());
    }

    public void deleteUserLunchByLunch(Lunch lunch) throws SQLException
    {
        DeleteBuilder<UserLunch, Integer> deleteBuilder = deleteBuilder();
        deleteBuilder.where().eq(UserLunch.LUNCH_USER_LUNCH_FIELD_NAME, lunch);
        deleteBuilder.delete();
    }
}
