package com.WhoIsNext.managers.dao;

import android.util.Log;

import com.WhoIsNext.data.Group;
import com.WhoIsNext.data.Lunch;
import com.WhoIsNext.data.User;
import com.WhoIsNext.data.UserGroup;
import com.WhoIsNext.data.UserLunch;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: kirill.tupikov
 * Date: 8/6/13
 * Time: 5:23 PM
 * To change this template use File | Settings | File Templates.
 */
public class LunchDAO extends BaseDaoImpl<Lunch, Integer>
{
    public LunchDAO(ConnectionSource connectionSource, Class<Lunch> dataClass) throws SQLException
    {
        super(connectionSource, dataClass);
    }

    public void deleteLunchByGroup(Group group) throws SQLException
    {
        DeleteBuilder<Lunch, Integer> deleteBuilder = deleteBuilder();
        deleteBuilder.where().eq(Lunch.GROUP_ID, group);
        deleteBuilder.delete();
    }

    public ArrayList<Lunch> getAllLunches() throws SQLException
    {
        return new ArrayList<Lunch>(this.queryForAll());
    }

    public ArrayList<Lunch> getLunchByGroup(Group group) throws android.database.SQLException, java.sql.SQLException
    {
        QueryBuilder<Lunch, Integer> queryBuilder = queryBuilder();
        queryBuilder.where().eq(Lunch.GROUP_ID, group);
        PreparedQuery<Lunch> preparedQuery = queryBuilder.prepare();
        ArrayList<Lunch> lunches = new ArrayList<Lunch>(query(preparedQuery));
        return lunches;
    }

    public ArrayList<Lunch> getLunchByIndex(int index) throws android.database.SQLException, java.sql.SQLException
    {
        QueryBuilder<Lunch, Integer> queryBuilder = queryBuilder();
        queryBuilder.where().eq("id", index);
        PreparedQuery<Lunch> preparedQuery = queryBuilder.prepare();
        ArrayList<Lunch> lunches = new ArrayList<Lunch>(query(preparedQuery));
        return lunches;
    }

    public ArrayList<Lunch> getLunchesByLastMonth(Date date)
    {
        //Log.info("CallDayPlanningDao", date.toString());
        //GenericRawResults<Object[]> rawResults = null;
//        Dao callDayPlanningDao = getDao(CallDayPlanning.class);
//        QueryBuilder query = callDayPlanningDao.queryBuilder();
        int year = date.getYear();
        int month = date.getMonth();
        Date dateLast = new Date(year, month - 1, 1);
//        Date date2 = new Date(year, month + 1, 1);

        Date startDate = new Date(dateLast.getTime() - 5);
//        Date endDate = new Date(date2.getTime() - 5);

        QueryBuilder<Lunch, Integer> queryBuilder = queryBuilder();
        ArrayList<Lunch> goalList = null;
        try
        {
            queryBuilder.where().between(Lunch.DATE_FIELD_NAME, startDate, date);
            PreparedQuery<Lunch> preparedQuery = queryBuilder.prepare();
            goalList = new ArrayList<Lunch>(query(preparedQuery));
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return goalList;

//        try
//        {
//            **query.where().between("calldate", startDate, endDate);**//This line is not working
//            if (status == Constant.cnStatus)
//            {
//                query.where().in("callstatus", status, Constant.ccStatus);
//            }
//            else
//            {
//                query.where().eq("callstatus", status);
//            }
//            query.groupBy("calldate");
//            query.selectRaw("calldate,count(*)");
//            rawResults = callDayPlanningDao.queryRaw(query.prepareStatementString(), new DataType[]{
//                    DataType.DATE_STRING, DataType.INTEGER});
//            // page through the results
//
//        }
//        catch (SQLException e1)
//        {
//            // TODO Auto-generated catch block
//            e1.printStackTrace();
//        }
//        return rawResults;
    }
}
