package com.WhoIsNext.managers.dao;

import com.WhoIsNext.data.Group;
import com.WhoIsNext.data.Lunch;
import com.WhoIsNext.data.UserGroup;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by kirill.tupikov on 7/11/13.
 */
public class UserGroupDAO extends BaseDaoImpl<UserGroup, Integer>
{

    public UserGroupDAO(ConnectionSource connectionSource, Class<UserGroup> dataClass) throws SQLException
    {
        super(connectionSource, dataClass);
    }

    public ArrayList<UserGroup> getAllUserGroups() throws SQLException
    {
        return new ArrayList<UserGroup>(this.queryForAll());
    }

    public ArrayList<UserGroup> getUserGroupByIndex(int index) throws android.database.SQLException, java.sql.SQLException
    {
        QueryBuilder<UserGroup, Integer> queryBuilder = queryBuilder();
        queryBuilder.where().eq(UserGroup.INDEX_FIELD_NAME, index);
        PreparedQuery<UserGroup> preparedQuery = queryBuilder.prepare();
        ArrayList<UserGroup> userGroups = new ArrayList<UserGroup>(query(preparedQuery));
        return userGroups;
    }

    public void deleteUserGroupByGroup(Group group) throws SQLException
    {
        DeleteBuilder<UserGroup, Integer> deleteBuilder = deleteBuilder();
        deleteBuilder.where().eq(UserGroup.GROUP_FIELD_NAME, group);
        deleteBuilder.delete();
    }

}
