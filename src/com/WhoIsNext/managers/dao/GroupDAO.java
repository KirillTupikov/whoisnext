package com.WhoIsNext.managers.dao;

import com.WhoIsNext.data.Group;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: kirill.tupikov
 * Date: 8/6/13
 * Time: 5:23 PM
 * To change this template use File | Settings | File Templates.
 */
public class GroupDAO extends BaseDaoImpl<Group, Integer>
{
    public GroupDAO(ConnectionSource connectionSource, Class<Group> dataClass) throws SQLException
    {
        super(connectionSource, dataClass);
    }

    public ArrayList<Group> getAllGroup() throws SQLException
    {
        return new ArrayList<Group>(this.queryForAll());
    }

    public ArrayList<Group> getGroupByIndex(int index) throws android.database.SQLException, java.sql.SQLException
    {
        QueryBuilder<Group, Integer> queryBuilder = queryBuilder();
        queryBuilder.where().eq(Group.GROUP_ID, index);
        PreparedQuery<Group> preparedQuery = queryBuilder.prepare();
        ArrayList<Group> groups = new ArrayList<Group>(query(preparedQuery));
        return groups;
    }
}
