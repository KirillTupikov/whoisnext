package com.WhoIsNext.managers.dao;

import com.WhoIsNext.data.Group;
import com.WhoIsNext.data.Lunch;
import com.WhoIsNext.data.User;
import com.WhoIsNext.data.UserContact;
import com.WhoIsNext.data.UserGroup;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by kirill.tupikov on 7/11/13.
 */
public class UserDAO extends BaseDaoImpl<User, Integer>
{
    public UserDAO(ConnectionSource connectionSource, Class<User> dataClass) throws SQLException
    {
        super(connectionSource, dataClass);
    }

    public ArrayList<User> getAllUsers() throws SQLException
    {
        return new ArrayList<User>(this.queryForAll());
    }

    public ArrayList<User> getAllUsersByContact(UserContact userContact) throws android.database.SQLException, java.sql.SQLException
    {
        QueryBuilder<User, Integer> queryBuilder = queryBuilder();
        queryBuilder.where().eq(User.USER_CONTACT_FIELD_NAME, userContact);
        PreparedQuery<User> preparedQuery = queryBuilder.prepare();
        ArrayList<User> users = new ArrayList<User>(query(preparedQuery));
        return users;
    }

    public ArrayList<User> getUserByIndex(int index) throws android.database.SQLException, java.sql.SQLException
    {
        QueryBuilder<User, Integer> queryBuilder = queryBuilder();
        queryBuilder.where().eq(User.INDEX_FIELD_NAME, index);
        PreparedQuery<User> preparedQuery = queryBuilder.prepare();
        ArrayList<User> users = new ArrayList<User>(query(preparedQuery));
        return users;
    }

    public void createUser(User user) throws SQLException
    {
        try
        {
            createOrUpdate(user);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

}
