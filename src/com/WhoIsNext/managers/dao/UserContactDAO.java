package com.WhoIsNext.managers.dao;

import com.WhoIsNext.data.UserContact;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Helm
 * Date: 27.09.13
 * Time: 11:50
 * To change this template use File | Settings | File Templates.
 */
public class UserContactDAO extends BaseDaoImpl<UserContact, Integer>
{
    public UserContactDAO(ConnectionSource connectionSource, Class<UserContact> dataClass) throws SQLException
    {
        super(connectionSource, dataClass);
    }

    public ArrayList<UserContact> getAllUsersContacts() throws SQLException
    {
        return new ArrayList<UserContact>(this.queryForAll());
    }

    public ArrayList<UserContact> getUserContactByName(String name) throws android.database.SQLException, java.sql.SQLException
    {
        QueryBuilder<UserContact, Integer> queryBuilder = queryBuilder();
        queryBuilder.where().eq(UserContact.USER_NAME_FIELD_NAME, name);
        PreparedQuery<UserContact> preparedQuery = queryBuilder.prepare();
        ArrayList<UserContact> userContacts = new ArrayList<UserContact>(query(preparedQuery));
        return userContacts;
    }

    public ArrayList<UserContact> getUserContactByIndexContact(String index) throws android.database.SQLException, java.sql.SQLException
    {
        QueryBuilder<UserContact, Integer> queryBuilder = queryBuilder();
        queryBuilder.where().eq(UserContact.USER_CONTACT_ID_FIELD_NAME, index);
        PreparedQuery<UserContact> preparedQuery = queryBuilder.prepare();
        ArrayList<UserContact> userContacts = new ArrayList<UserContact>(query(preparedQuery));
        return userContacts;
    }

    public ArrayList<UserContact> getUserContactByIndex(String index) throws android.database.SQLException, java.sql.SQLException
    {
        QueryBuilder<UserContact, Integer> queryBuilder = queryBuilder();
        queryBuilder.where().eq(UserContact.USER_ID_FIELD_NAME, index);
        PreparedQuery<UserContact> preparedQuery = queryBuilder.prepare();
        ArrayList<UserContact> userContacts = new ArrayList<UserContact>(query(preparedQuery));
        return userContacts;
    }

    public ArrayList<UserContact> getUserContactByInputText(String query) throws android.database.SQLException, java.sql.SQLException
    {
        QueryBuilder<UserContact, Integer> queryBuilder = queryBuilder();
        queryBuilder.where().like(UserContact.USER_NAME_FIELD_NAME, "%" + query + "%");
        PreparedQuery<UserContact> preparedQuery = queryBuilder.prepare();
        ArrayList<UserContact> userContacts = new ArrayList<UserContact>(query(preparedQuery));
        return userContacts;
    }
}
