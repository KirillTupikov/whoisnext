package com.WhoIsNext.managers;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import com.WhoIsNext.Settings;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * Created by Helm on 03.10.13.
 */
public class APIManager
{
    public static boolean checkConnection()
    {
        try
        {

            processRequest(new URI(Settings.NORMAL_LOG_URL_KEY));
            return true;
        }
        catch (Exception e)
        {
            return false;
        }
    }

    private static Session createSessionObject()
    {
        Properties properties = new Properties();
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", "smtp.gmail.com");
        properties.put("mail.smtp.port", "587");

        return Session.getInstance(properties, new javax.mail.Authenticator()
        {
            protected PasswordAuthentication getPasswordAuthentication()
            {
                return new PasswordAuthentication("helmt89", "543396dfnDFVdfkthf");
            }
        });
    }

    private static void sendMail(String email, String subject, String messageBody)
    {
        Session session = createSessionObject();

        try
        {
            Message message = createMessage(email, subject, messageBody, session);
             new SendMailTask().execute(message);
        }
        catch (AddressException e)
        {
            e.printStackTrace();
        }
        catch (MessagingException e)
        {
            e.printStackTrace();
        }
        catch (UnsupportedEncodingException e)
        {
            e.printStackTrace();
        }
    }

    private static Message createMessage(String email, String subject, String messageBody, Session session) throws MessagingException, UnsupportedEncodingException
    {
        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress("helmt89@gmail.com", "Tiemen Schut"));
        message.addRecipient(Message.RecipientType.TO, new InternetAddress(email, email));
        message.setSubject(subject);
        message.setText(messageBody);
        return message;
    }

    private static class SendMailTask extends AsyncTask<Message, Void, Void>
    {
        //private ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //progressDialog = ProgressDialog.show(MainActivity.this, "Please wait", "Sending mail", true, false);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            //progressDialog.dismiss();
        }

        @Override
        protected Void doInBackground(Message... messages) {
            try {
                Transport.send(messages[0]);
            } catch (MessagingException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public static void sendComplaint(String subject, String text, String fromEmail, String toEmail, Context context)
    {

        sendMail(toEmail, subject, text);
//        Intent email = new Intent(Intent.ACTION_SEND);
//        email.putExtra(Intent.EXTRA_EMAIL, new String[]{toEmail});
//        // email.putExtra(Intent.EXTRA_CC, new String[]{ to});
//        email.putExtra(Intent.EXTRA_BCC, new String[]{fromEmail});
//        email.putExtra(Intent.EXTRA_SUBJECT, subject);
//        email.putExtra(Intent.EXTRA_TEXT, text);
//
//        // need this to prompts email client only
//        email.setType("message/rfc822");
//
//        context.startActivity(Intent.createChooser(email, "Choose an Email client :"));


    }

    private static InputStream processRequest(URI uri) throws Exception
    {
        HttpClient httpClient = ConnectionManager.getHttpClient();
        HttpGet method = new HttpGet(uri);
        method.setHeader("Content-Type", "application/x-www-form-urlencoded");
        HttpResponse httpResponse = httpClient.execute(method);
        HttpEntity httpEntity = httpResponse.getEntity();
        InputStream inputStream = httpEntity.getContent();
        return inputStream;
    }

//
//    public static void sendComplaint(String name, String phone, String email, String text)
//    {
//
//
//        String request;
//        try
//        {
//            request = "Name=" + URLEncoder.encode(name) + "&Phone=" + URLEncoder.encode(phone) + "&Email=" + URLEncoder.encode(email) + "&Text=" + URLEncoder.encode(text, "utf-8");
//
//            // processPostRequest(ExternalSettingsManager.getStringValue(ExternalSettingsManager.NORMAL_COMPLAINT_URL_KEY), request);
//        }
//        catch (Exception e)
//        {
//            //LogManager.addRecord(e);
//        }
//    }

    private static InputStream processPostRequest(String url, String request) throws IOException
    {
        try
        {
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost method = new HttpPost(url);
            method.setHeader("Content-Type", "application/x-www-form-urlencoded");
            StringEntity entity = new StringEntity(request);
            method.setEntity(entity);
            httpClient.execute(method);
        }
        catch (Exception e)
        {
            //LogManager.addRecord(e);
        }
        return null;
    }

}
