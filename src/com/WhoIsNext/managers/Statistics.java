package com.WhoIsNext.managers;

import com.WhoIsNext.data.Group;
import com.WhoIsNext.data.User;
import com.WhoIsNext.data.UserContact;
import com.WhoIsNext.data.UserGroup;
import com.WhoIsNext.data.UserLunch;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by kirill.tupikov on 10/9/13.
 */
public class Statistics
{

    static public String nextAdmin(int index, boolean refreshAdmin) throws SQLException
    {

        Group group = DataBaseManager.getInstance().getGroupByIndex(index);

        int max = group.getUserGroups().get(0).getUser().getAllDebtSum();//////////пересмотреть!!!!!!!!!!!!!!!
        UserGroup userGroupMax = group.getUserGroups().get(0);
        //Инициализируем начальные параметры поиска
        for (UserGroup userGroup : group.getUserGroups())
        {
            //Первоначальные параметры максимумы не могут быть администратором либо заблокированным пользователем
            if (userGroup.getUser().getStatus().equals(User.USER_STATUS_PAY) || userGroup.getUser().getStatus().equals(User.USER_STATUS_NOT_PAY))
            {
                max = userGroup.getUser().getAllDebtSum();//////////пересмотреть!!!!!!!!!!!!!!!
                userGroupMax = userGroup;
                break;
            }
        }

        //находим самый большой долг
        for (UserGroup userGroup : group.getUserGroups())
        {
            if (!userGroup.getUser().getStatus().equals(User.USER_STATUS_DISABLED))//если пользователь не заблочен
            {
                if (userGroup.getUser().getStatus().equals(User.USER_STATUS_ADMIN_LUNCH) && refreshAdmin)
                {
                    //если идёт пересчёт админа, он не участвует в поиске максимальной задолженности
                    userGroup.getUser().setStatus(User.USER_STATUS_DISABLED);//?
                }
                else
                {
                    if (max < userGroup.getUser().getAllDebtSum() && !userGroup.getUser().getStatus().equals(User.USER_STATUS_DISABLED))
                    {
                        max = userGroup.getUser().getAllDebtSum();
                        userGroupMax = userGroup;
                    }
                    userGroup.getUser().setStatus(User.USER_STATUS_NOT_PAY);
//                    if (userGroup.getUser().getStatus().equals(User.USER_STATUS_ADMIN_LUNCH))
//                    {
//                        userGroup.getUser().setStatus(User.USER_STATUS_NOT_PAY);
//                    }
//                    else
//                        if (userGroup.getUser().getStatus().equals(User.USER_STATUS_PAY))//при завешении платежа обнуляем оплаты пользователей
//                            userGroup.getUser().setStatus(User.USER_STATUS_NOT_PAY);
                }

                userGroup.getUser().setDebtSum(0);

                DataBaseManager.getInstance().createUser(userGroup.getUser());
            }
        }

        userGroupMax.getUser().setStatus(User.USER_STATUS_ADMIN_LUNCH);
        DataBaseManager.getInstance().createUser(userGroupMax.getUser());

        return userGroupMax.getUser().getUserContact().getUserName();
    }

    //Получение средней оплаты за обед по всем группам для заданного контакта
    static public String getAverageSumAllLunchUser(UserContact userContact)
    {
        double averageSum = 0;
        int i = 0;
        ArrayList<User> users = new ArrayList<User>();//Достаём всех юзеров, связанных с данным контактом
        try
        {
            users = DataBaseManager.getInstance().getAllUsersByContact(userContact);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        for (User user : users)
        {
            for (UserLunch userLunch : user.getUserLunches())
            {
                i++;
                averageSum = averageSum + userLunch.getPaymentSum();
            }
        }

        if (averageSum != 0.0)
        {
            averageSum = new BigDecimal(averageSum / i).setScale(2, RoundingMode.UP).doubleValue();
            return String.valueOf(averageSum);
        }
        else
            return "0.0";
    }

    //Получение общей суммы на еду за последний месяц по всем группам для заданного контакта
    static public String getDebSumLastMonth(UserContact userContact)
    {
        ArrayList<User> users = new ArrayList<User>();//Достаём всех юзеров, связанных с данным контактом
        try
        {
            users = DataBaseManager.getInstance().getAllUsersByContact(userContact);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        final Calendar c = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM-dd-yyyy HH:mm");
        String date = simpleDateFormat.format(c.getTimeInMillis());
        Date dateTime = null;
        try
        {
            dateTime = simpleDateFormat.parse(date);
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }

        int year = dateTime.getYear();
        int month = dateTime.getMonth();
        Date dateLast = new Date(year, month - 1, 1);

        double sumLastMonth = 0;

        for (User user : users)
        {
            for (UserLunch userLunch : user.getUserLunches())
            {
                if (userLunch.getLunch().getDate().getTime() > dateLast.getTime())//date1.getTime() < date2.getTime()
                {
                    sumLastMonth = sumLastMonth + userLunch.getPaymentSum();
                }
            }
        }

        if (sumLastMonth != 0)
            return String.valueOf(sumLastMonth);
        else
            return "0";
    }

    //Получение общей задолженности по всем группам
    static public String getAllGroupDebSum(UserContact userContact)
    {
        double allDebSum = 0;
        ArrayList<User> users = new ArrayList<User>();//Достаём всех юзеров, связанных с данным контактом
        try
        {
            users = DataBaseManager.getInstance().getAllUsersByContact(userContact);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        for (User user : users)
        {
            allDebSum = allDebSum + user.getAllDebtSum();
        }

        return String.valueOf(allDebSum);
    }


    //Получение средней оплаты за обед по всем группам для заданного контакта
    static public String getAverageSumLunchUser(User user)
    {
        double averageSum = 0;
        int i = 0;

        for (UserLunch userLunch : user.getUserLunches())
        {
            i++;
            averageSum = averageSum + userLunch.getPaymentSum();
        }

        if (averageSum != 0.0)
        {
            averageSum = new BigDecimal(averageSum / i).setScale(2, RoundingMode.UP).doubleValue();
            return String.valueOf(averageSum);
        }
        else
            return "0.0";
    }

}
