package com.WhoIsNext.managers;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Shader;
import android.net.Uri;
import android.os.Environment;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by kirill.tupikov on 10/1/13.
 */
public class ImageManager
{

    private static File directory;

    static private ImageManager instance;

    public static void init()
    {
        if (instance == null)
            instance = new ImageManager();

    }

    public static ImageManager getInstance()
    {
        return instance;
    }

    public ImageManager()
    {
        createDirectory();
    }

    public Bitmap loadContactPhoto(ContentResolver cr, long id)
    {
        Uri uri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, id);
        InputStream input = ContactsContract.Contacts.openContactPhotoInputStream(cr, uri);
        if (input == null)
        {
            return null;
        }
        return BitmapFactory.decodeStream(input);
    }

    public Bitmap loadImageByPath(String path)
    {
        return BitmapFactory.decodeFile(getDirectory() + "/" + path);
    }

    public Bitmap resizeImage(Bitmap bitmap, int maxSize)
    {
        if (bitmap.getWidth() > maxSize)
        {
            int newWidth;
            int newHeight;

            if (bitmap.getWidth() > bitmap.getHeight())
            {
                newWidth = maxSize - 37;
                newHeight = (int) ((double) newWidth / (double) bitmap.getWidth() * bitmap.getHeight());
            }
            else
            {
                newHeight = maxSize - 37;
                newWidth = (int) ((double) newHeight / (double) bitmap.getHeight() * bitmap.getWidth());
            }
            return Bitmap.createScaledBitmap(bitmap, newWidth, newHeight, true);
        }
        else
            return bitmap;
    }

    public Bitmap circleAvatar(Bitmap bitmap)
    {
        Bitmap circleBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_4444);
        BitmapShader shader = new BitmapShader(bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);

        Paint paint = new Paint();
        paint.setShader(shader);
        paint.setAntiAlias(true);
        paint.setFilterBitmap(true);
        paint.setDither(true);

        Canvas c = new Canvas(circleBitmap);

        float radius;
        if (bitmap.getWidth() < bitmap.getHeight())
            radius = bitmap.getWidth() / 2;
        else
            radius = bitmap.getHeight() / 2;

        c.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2, radius, paint);

        return circleBitmap;
    }

    public void imageLoader(String imageUrl, ImageView imageView, Context context)
    {
        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
        imageLoader.displayImage(imageUrl, imageView);
    }

    public String saveImageFromUri(String uri, Context context)
    {
        try
        {
            URL url = new URL(uri);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return saveGenerateFileUri(myBitmap, context);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    public String saveGenerateFileUri(Bitmap bitmap, Context context) throws IOException
    {
        OutputStream fOut = null;
        File file = new File(directory.getPath() + "/" + "photo_" + System.currentTimeMillis() + ".jpg");
        Log.d("Tag", "fileName = " + file);

        fOut = new FileOutputStream(file);

        bitmap.compress(Bitmap.CompressFormat.JPEG, 85, fOut);
        fOut.flush();
        fOut.close();

        MediaStore.Images.Media.insertImage(context.getContentResolver(), file.getAbsolutePath(), file.getName(), file.getName());

        return file.getName();
    }

    public boolean deleteImage(String filePath)
    {
        File file = new File(directory.getPath() + "/" + filePath);
        if (file.exists())
        {
            file.delete();
            return true;
        }
        else
            return false;
    }

    private File createDirectory()
    {
        directory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "ImageWhoIsNext");
        if (!directory.exists())
            directory.mkdirs();
        return directory;
    }

    public File getDirectory()
    {
        return directory;
    }
}
