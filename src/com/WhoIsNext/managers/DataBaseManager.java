package com.WhoIsNext.managers;

import android.content.Context;

import com.WhoIsNext.data.*;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by kirill.tupikov on 7/11/13.
 */
public class DataBaseManager
{
    static private DataBaseManager instance;

    private DataBaseHelper dataBaseHelper;

    static public void init(Context context)
    {
        if (instance == null)
        {
            instance = new DataBaseManager(context);
        }
    }

    static public DataBaseManager getInstance()
    {
        return instance;
    }

    public DataBaseManager(Context context)
    {
        dataBaseHelper = new DataBaseHelper(context);
    }

    public DataBaseHelper getDataBaseHelper()
    {
        return dataBaseHelper;
    }

    public void createUser(User user) throws SQLException
    {
        dataBaseHelper.getUserDAO().createUser(user);
    }

    public void createUserGroup(UserGroup userGroup) throws SQLException
    {
        dataBaseHelper.getUserGroupDAO().createOrUpdate(userGroup);
    }

    public ArrayList<User> getAllUsers() throws java.sql.SQLException
    {
        return dataBaseHelper.getUserDAO().getAllUsers();
    }

    public ArrayList<User> getAllUsersByContact(UserContact userContact) throws java.sql.SQLException
    {
        return dataBaseHelper.getUserDAO().getAllUsersByContact(userContact);
    }

    public ArrayList<UserGroup> getAllUserGroup() throws java.sql.SQLException
    {
        return dataBaseHelper.getUserGroupDAO().getAllUserGroups();
    }

    public ArrayList<Lunch> getAllLunches() throws SQLException
    {
        return dataBaseHelper.getLunchDAO().getAllLunches();
    }

    public ArrayList<Group> getAllGroup() throws SQLException
    {
        return dataBaseHelper.getGroupDAO().getAllGroup();
    }

    public ArrayList<UserLunch> getUserLunches() throws SQLException
    {
        return dataBaseHelper.getUserLunchDAO().getAllUserLunches();
    }

    public void createGroup(Group group) throws SQLException
    {
        dataBaseHelper.getGroupDAO().createOrUpdate(group);
    }

    public void createLunch(Lunch lunch) throws SQLException
    {
        dataBaseHelper.getLunchDAO().createOrUpdate(lunch);
    }

    public void createUserLunch(UserLunch userLunch) throws SQLException
    {
        dataBaseHelper.getUserLunchDAO().createOrUpdate(userLunch);
    }


    public User getUserByIndex(int index) throws java.sql.SQLException
    {
        return dataBaseHelper.getUserDAO().getUserByIndex(index).get(0);
    }

    public ArrayList<UserGroup> getUserGroupByIndex(int index) throws java.sql.SQLException
    {
        return dataBaseHelper.getUserGroupDAO().getUserGroupByIndex(index);
    }

    public void deleteGroup(Group group) throws SQLException
    {
        //Вычищаем данные, связанные с этой группой!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!подумать
//        for (UserGroup userGroup : group.getUserGroups())//удаляем всех юзеров, связанных с группой
//        {
//            dataBaseHelper.getUserDAO().delete(userGroup.getUser());
//        }
//        dataBaseHelper.getUserGroupDAO().deleteUserGroupByGroup(group);//удаляем всех UserGroup
//
//        for (Lunch lunch : dataBaseHelper.getLunchDAO().getLunchByGroup(group))//удаляем все UserLunch для каждого ланча
//        {
//            dataBaseHelper.getUserLunchDAO().deleteUserLunchByLunch(lunch);
//        }
//        dataBaseHelper.getLunchDAO().deleteLunchByGroup(group);//удаляем все Lunch

        dataBaseHelper.getGroupDAO().delete(group);//удаляем группу
    }

    public void deleteUserGroup(UserGroup userGroup) throws SQLException
    {
        dataBaseHelper.getUserGroupDAO().delete(userGroup);
    }

    public Group getGroupByIndex(int index) throws SQLException
    {
        return dataBaseHelper.getGroupDAO().getGroupByIndex(index).get(0);
    }

    public ArrayList<Lunch> getLunchesByGroup(Group group) throws SQLException
    {
        return dataBaseHelper.getLunchDAO().getLunchByGroup(group);
    }

    public Lunch getLunchByIndex(int index) throws java.sql.SQLException
    {
        return dataBaseHelper.getLunchDAO().getLunchByIndex(index).get(0);
    }

    public void createUserContacts(UserContact userContact) throws SQLException
    {
        dataBaseHelper.getUserContactDAO().createOrUpdate(userContact);
    }

    public void createOrUpdateUserContacts(UserContact userContact) throws SQLException
    {
        dataBaseHelper.getUserContactDAO().createOrUpdate(userContact);
    }

    public ArrayList<UserContact> getAllUserContact() throws SQLException
    {
        return dataBaseHelper.getUserContactDAO().getAllUsersContacts();
    }

    public UserContact getUserContactByName(String name) throws SQLException
    {
        return dataBaseHelper.getUserContactDAO().getUserContactByName(name).get(0);
    }

    public UserContact getUserContactByIndexContact(String index) throws SQLException
    {
        return dataBaseHelper.getUserContactDAO().getUserContactByIndexContact(index).get(0);
    }

    public UserContact getUserContactByIndex(String index) throws SQLException
    {
        return dataBaseHelper.getUserContactDAO().getUserContactByIndex(index).get(0);
    }

    public ArrayList<UserContact> getUserContactsByInputText(String query) throws SQLException
    {
        return dataBaseHelper.getUserContactDAO().getUserContactByInputText(query);
    }
}
