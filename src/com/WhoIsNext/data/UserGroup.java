package com.WhoIsNext.data;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;
import org.w3c.dom.ls.LSSerializer;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by kirill.tupikov on 7/8/13.
 */

@DatabaseTable(tableName = "users")
public class UserGroup implements Serializable
{
    public static final String INDEX_FIELD_NAME = "id";
    public static final String GROUP_FIELD_NAME = "group";

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(foreign = true, foreignAutoRefresh = true)
    private User user;

    @DatabaseField(foreign = true, foreignAutoRefresh = true)
    private Group group;


    public UserGroup()
    {
    }

    public UserGroup(User user)
    {
        this.user = user;
    }

    public Group getGroup()
    {
        return group;
    }

    public void setGroup(Group group)
    {
        this.group = group;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public User getUser()
    {
        return user;
    }

    public void setUser(User user)
    {
        this.user = user;
    }
}
