package com.WhoIsNext.data;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by kirill.tupikov on 7/9/13.
 */
@DatabaseTable(tableName = "user")
public class User implements Serializable, Cloneable
{
    public static final String USER_ID = "user_id";

    public static final String USER_STATUS_PAY = "user_status_pay";
    public static final String USER_STATUS_NOT_PAY = "user_status_not_pay";
    public static final String USER_STATUS_DISABLED = "user_status_disabled";
    public static final String USER_STATUS_ADMIN_LUNCH = "user_status_admin_lunch";


    public static final String INDEX_FIELD_NAME = "id";
    public static final String USER_CONTACT_FIELD_NAME = "user_contact";
    public static final String DEBT_SUM_FIELD_NAME = "debt_sum";
    public static final String ALL_DEBT_SUM_FIELD_NAME = "all_debt_sum";
    public static final String STATUS_FIELD_NAME = "status";

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(dataType = DataType.INTEGER, columnName = DEBT_SUM_FIELD_NAME)
    private int debtSum;

    @DatabaseField(dataType = DataType.INTEGER, columnName = ALL_DEBT_SUM_FIELD_NAME)
    private int allDebtSum;

    @DatabaseField(dataType = DataType.STRING, columnName = STATUS_FIELD_NAME)
    private String status;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = USER_CONTACT_FIELD_NAME)
    private UserContact userContact;

    @ForeignCollectionField(eager = true)
    private ForeignCollection<UserGroup> userGroups;

    @ForeignCollectionField(eager = true)
    private ForeignCollection<UserLunch> userLunches;


    public User()
    {

    }

    public User(int debtSum, int allDebtSum, String status, UserContact userContact)
    {
        this.debtSum = debtSum;
        this.allDebtSum = allDebtSum;
        this.status = status;
        this.userContact = userContact;
    }


    @Override
    public User clone() throws CloneNotSupportedException
    {
        return (User) super.clone();
    }

    public UserContact getUserContact()
    {
        return userContact;
    }

    public void setUserContact(UserContact userContact)
    {
        this.userContact = userContact;
    }

    public ForeignCollection<UserGroup> getUserGroups()
    {
        return userGroups;
    }

    public void setUserGroups(ForeignCollection<UserGroup> userGroups)
    {
        this.userGroups = userGroups;
    }

    public ForeignCollection<UserLunch> getUserLunches()
    {
        return userLunches;
    }

    public void setUserLunches(ForeignCollection<UserLunch> userLunches)
    {
        this.userLunches = userLunches;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public int getDebtSum()
    {
        return debtSum;
    }

    public void setDebtSum(int debtSum)
    {
        this.debtSum = debtSum;
    }

    public int getAllDebtSum()
    {
        return allDebtSum;
    }

    public void setAllDebtSum(int allDebtSum)
    {
        this.allDebtSum =  this.allDebtSum + allDebtSum;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }
}
