package com.WhoIsNext.data;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: kirill.tupikov
 * Date: 9/26/13
 * Time: 6:46 PM
 * To change this template use File | Settings | File Templates.
 */
public class UserContact implements Serializable
{
    public static final String USER_ID_FIELD_NAME = "id";
    public static final String USER_CONTACT_ID_FIELD_NAME = "user_contact_id";
    public static final String USER_NAME_FIELD_NAME = "user_name";
    public static final String USER_PHONE_FIELD_NAME = "user_phone";
    public static final String USER_PHOTO_FIELD_NAME = "user_photo";

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(dataType = DataType.STRING, columnName = USER_CONTACT_ID_FIELD_NAME)
    private String userContactId;

    @DatabaseField(dataType = DataType.STRING, columnName = USER_NAME_FIELD_NAME)
    private String userName;

    @DatabaseField(dataType = DataType.STRING, columnName = USER_PHONE_FIELD_NAME)
    private String userPhone;

    @DatabaseField(dataType = DataType.STRING, columnName = USER_PHOTO_FIELD_NAME)
    private String userPhoto;

    private boolean addedToGroup;

    public UserContact()
    {

    }

    public UserContact(String userContactId, String userName, String userPhone, String userPhoto)
    {
        this.userContactId = userContactId;
        this.userName = userName;
        this.userPhone = userPhone;
        this.userPhoto = userPhoto;
        addedToGroup = false;
    }

    public boolean isAddedToGroup()
    {
        return addedToGroup;
    }

    public void setAddedToGroup(boolean addedToGroup)
    {
        this.addedToGroup = addedToGroup;
    }

    public void clickAddedToGroup()
    {
        addedToGroup = !addedToGroup;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getUserContactId()
    {
        return userContactId;
    }

    public void setUserContactId(String userContactId)
    {
        this.userContactId = userContactId;
    }

    public String getUserPhoto()
    {
        return userPhoto;
    }

    public void setUserPhoto(String userPhoto)
    {
        this.userPhoto = userPhoto;
    }

    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public String getUserPhone()
    {
        return userPhone;
    }

    public void setUserPhone(String userPhone)
    {
        this.userPhone = userPhone;
    }
}
