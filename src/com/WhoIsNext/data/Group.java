package com.WhoIsNext.data;

import com.WhoIsNext.managers.DataBaseManager;
import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: kirill.tupikov
 * Date: 8/5/13
 * Time: 5:15 PM
 * To change this template use File | Settings | File Templates.
 */

@DatabaseTable(tableName = "group")
public class Group implements Serializable
{

    public final static String GROUP_NAME_FIELD_NAME = "group_name";
    public static final String STATUS_OF_GROUP_FIELD_NAME = "status_of_group";
    public final static String GROUP_PHOTO_FIELD_NAME = "group_photo";
    public static final String GROUP_ID = "id";

    public static final String STATUS_PARTY_ENABLED = "status_company_enabled";
    public static final String STATUS_PARTY_DISABLED = "status_party_disabled";

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(dataType = DataType.STRING, columnName = GROUP_NAME_FIELD_NAME)
    private String groupName;

    @DatabaseField(dataType = DataType.STRING, columnName = STATUS_OF_GROUP_FIELD_NAME)
    private String status;

    @DatabaseField(dataType = DataType.STRING, columnName = GROUP_PHOTO_FIELD_NAME)
    private String groupPhoto;

    @ForeignCollectionField(eager = true)
    private ForeignCollection<UserGroup> userGroups;

    public Group()
    {
    }

    public Group(String groupName, String status)
    {
        this.groupName = groupName;
        this.status = status;
    }

    public String getGroupPhoto()
    {
        return groupPhoto;
    }

    public void setGroupPhoto(String groupPhoto)
    {
        this.groupPhoto = groupPhoto;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public void removeUserGroup(UserGroup value) throws SQLException
    {
        userGroups.remove(value);
        DataBaseManager.getInstance().getDataBaseHelper().getUserGroupDAO().delete(value);
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getGroupName()
    {
        return groupName;
    }

    public void setGroupName(String groupName)
    {
        this.groupName = groupName;
    }

    public ArrayList<UserGroup> getUserGroups()
    {
        ArrayList<UserGroup> uG = new ArrayList<UserGroup>();
        for (UserGroup userGroup : userGroups)
        {
            uG.add(userGroup);
        }
        return uG;
    }

    public ArrayList<UserGroup> getActiveUserGroups()
    {
        ArrayList<UserGroup> uG = new ArrayList<UserGroup>();
        for (UserGroup userGroup : userGroups)
        {
            if (!userGroup.getUser().getStatus().equals(User.USER_STATUS_DISABLED))//если пользователи активны
                uG.add(userGroup);
        }
        return uG;
    }

    public void setUserGroups(ForeignCollection<UserGroup> userGroups)
    {
        this.userGroups = userGroups;
    }

    public void addUserGroup(UserGroup userGroup)
    {
        userGroups.add(userGroup);
    }
}
