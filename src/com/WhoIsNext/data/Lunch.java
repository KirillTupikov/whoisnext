package com.WhoIsNext.data;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: kirill.tupikov
 * Date: 8/5/13
 * Time: 5:14 PM
 * To change this template use File | Settings | File Templates.
 */
@DatabaseTable(tableName = "lunch")
public class Lunch implements Serializable
{
    public final static String DATE_FIELD_NAME = "date_lunch";
    public final static String IMAGE_CHECK_FIELD_NAME = "image_check";
    public static final String SUM_LUNCH_FIELD_NAME = "sum_lunch";
    public static final String GROUP_ID = "group_id";

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(dataType = DataType.DATE, columnName = DATE_FIELD_NAME)
    private Date date;

    @DatabaseField(dataType = DataType.DOUBLE, columnName = SUM_LUNCH_FIELD_NAME)
    private double sumLunch;

    @DatabaseField(id = false, foreign = false, columnName = IMAGE_CHECK_FIELD_NAME, dataType = DataType.SERIALIZABLE)
    private ArrayList<String> imageCheck;


    @ForeignCollectionField(eager = true)
    private ForeignCollection<UserLunch> userLunches;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = GROUP_ID)
    private Group group;

    public Lunch()
    {
    }

    public Lunch(Date date, double sumLunch, Group group)
    {
        this.date = date;
        this.sumLunch = sumLunch;
        this.group = group;
        imageCheck = new ArrayList<String>();
    }


    public Group getGroup()
    {
        return group;
    }

    public void setGroup(Group group)
    {
        this.group = group;
    }

    public ArrayList<String> getImageCheck()
    {
        return imageCheck;
    }

    public void setImageCheck(ArrayList<String> imageCheck)
    {
        this.imageCheck = imageCheck;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public Date getDate()
    {
        return date;
    }

    public void setDate(Date date)
    {
        this.date = date;
    }

    public double getSumLunch()
    {
        return sumLunch;
    }

    public void setSumLunch(double sumLunch)
    {
        this.sumLunch = sumLunch;
    }

    public ArrayList<UserLunch> getUserLunches()
    {
        ArrayList<UserLunch> uG = new ArrayList<UserLunch>();
        for (UserLunch userLunch : userLunches)
        {
            uG.add(userLunch);
        }
        return uG;
    }

    public ArrayList<UserLunch> getPaymentUserLunches()
    {
        ArrayList<UserLunch> userLunchesBuf = new ArrayList<UserLunch>();

        for (UserLunch userLunch : userLunches)
        {
            if(userLunch.getPaymentSum() != 0)
                userLunchesBuf.add(userLunch);
        }
        return userLunchesBuf;
    }

    public void setUserLunches(ForeignCollection<UserLunch> userLunches)
    {
        this.userLunches = userLunches;
    }
}
