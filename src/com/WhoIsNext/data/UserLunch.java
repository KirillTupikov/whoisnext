package com.WhoIsNext.data;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: kirill.tupikov
 * Date: 8/5/13
 * Time: 5:14 PM
 * To change this template use File | Settings | File Templates.
 */
@DatabaseTable(tableName = "user_lunch")
public class UserLunch implements Serializable
{
    public static final String ID_USER_LUNCH_FIELD_NAME = "id";
    public static final String PAYMENT_SUM_FIELD_NAME = "payment_sum";
    public static final String STATUS_LUNCH_FIELD_NAME = "status_lunch";
    public static final String LUNCH_USER_LUNCH_FIELD_NAME = "lunch";

    @DatabaseField(generatedId = true, columnName = ID_USER_LUNCH_FIELD_NAME)
    private int id;

    @DatabaseField(dataType = DataType.INTEGER, columnName = PAYMENT_SUM_FIELD_NAME)
    private int paymentSum;

    @DatabaseField(dataType = DataType.STRING, columnName = STATUS_LUNCH_FIELD_NAME)
    private String status;

    @DatabaseField(foreign = true, foreignAutoRefresh = true)
    private User user;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = LUNCH_USER_LUNCH_FIELD_NAME)
    private Lunch lunch;

    public UserLunch()
    {

    }

    public UserLunch(int paymentSum, String status, User user, Lunch lunch)
    {
        this.paymentSum = paymentSum;
        this.status = status;
        this.user = user;
        this.lunch = lunch;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public User getUser()
    {
        return user;
    }

    public void setUser(User user)
    {
        this.user = user;
    }

    public Lunch getLunch()
    {
        return lunch;
    }

    public void setLunch(Lunch lunch)
    {
        this.lunch = lunch;
    }

    public int getPaymentSum()
    {
        return paymentSum;
    }

    public void setPaymentSum(int paymentSum)
    {
        this.paymentSum = paymentSum;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }
}
