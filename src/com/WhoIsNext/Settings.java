package com.WhoIsNext;

/**
 * Created with IntelliJ IDEA.
 * User: kirill.tupikov
 * Date: 9/20/13
 * Time: 4:08 PM
 * To change this template use File | Settings | File Templates.
 */
public class Settings
{
    public static final int ACTIVITY_EDIT_SUM_BUTTON_COD_REQUEST = 1;
    public static final int ACTIVITY_EDIT_SUM_ITEM_MENU_COD_REQUEST = 2;
    public static final int ACTIVITY_SNAPSHOT_COD_REQUEST = 3;
    public static final int ACTIVITY_ADD_USER_COD_REQUEST = 4;
    public static final int ACTIVITY_SNAPSHOT_VIEW_COD_REQUEST = 5;
    public static final int REQUEST_CODE_PHOTO = 6;
    public static final int COUNT_OF_PHONE_NUMBERS = 9;
    public static final String NORMAL_LOG_URL_KEY = "http://google.com";
    public static final String CURRENCY_BEL = "currency_bel";
    public static final String CURRENCY_RUS = "currency_rus";
    public static final String CURRENCY_USD = "currency_usd";
    public static final String CURRENCY_EUR = "currency_eur";
    public static final String MAX_SUM = "237";
    public static final String API_ID = "3936804";
    public static final int AUTHORIZATION_VKONTAKTE_COD_REQUEST = 7;
    public static final int RESULT_LOAD_IMAGE = 8;

}
