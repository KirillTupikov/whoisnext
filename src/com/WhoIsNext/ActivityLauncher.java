package com.WhoIsNext;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.WhoIsNext.data.Group;
import com.WhoIsNext.data.Lunch;
import com.WhoIsNext.data.User;
import com.WhoIsNext.data.UserContact;
import com.WhoIsNext.form.activity.AboutProgramAct;
import com.WhoIsNext.form.activity.AddUserAct;
import com.WhoIsNext.form.activity.EditSumAct;
import com.WhoIsNext.form.activity.FacebookLoginAct;
import com.WhoIsNext.form.activity.HistoryLunchesAct;
import com.WhoIsNext.form.activity.MainAct;
import com.WhoIsNext.form.activity.MembersOfLunchAct;
import com.WhoIsNext.form.activity.PaymentLunchAct;
import com.WhoIsNext.form.activity.PersonalProfileAct;
import com.WhoIsNext.form.activity.PreferenceAct;
import com.WhoIsNext.form.activity.SnapshotViewAct;
import com.WhoIsNext.form.activity.SnapshotsAct;
import com.WhoIsNext.form.activity.SupportAct;
import com.WhoIsNext.form.activity.TwitterLoginAct;
import com.WhoIsNext.form.activity.UserStatisticsAct;
import com.WhoIsNext.form.activity.VKontakteLoginAct;

import java.util.ArrayList;

/**
 * Created by kirill.tupikov on 7/4/13.
 */
public class ActivityLauncher
{
    public static void showMainAct(Context context)
    {
        Intent intent = new Intent(context, MainAct.class);
        context.startActivity(intent);
    }

    public static void showLoginActForResult(Activity context, int requestCode)
    {
        Intent intent = new Intent(context, VKontakteLoginAct.class);
        context.startActivityForResult(intent, requestCode);
    }

    public static void showPaymentLunchAct(Context context, int partyIndex)
    {
        Intent intent = new Intent(context, PaymentLunchAct.class);
        intent.putExtra(PaymentLunchAct.PARTY_ITEM, partyIndex);
        context.startActivity(intent);
    }

    public static void showFacebookLoginAct(Context context)
    {
        Intent intent = new Intent(context, FacebookLoginAct.class);
        context.startActivity(intent);
    }

    public static void showTwitterLoginAct(Context context)
    {
        Intent intent = new Intent(context, TwitterLoginAct.class);
        context.startActivity(intent);
    }

    public static void showSnapshotsActForResult(Activity context, int requestCode, ArrayList<String> checks, String mode)
    {
        Intent intent = new Intent(context, SnapshotsAct.class);
        intent.putExtra(Lunch.IMAGE_CHECK_FIELD_NAME, checks);
        intent.putExtra(SnapshotsAct.MODE, mode);
        context.startActivityForResult(intent, requestCode);
    }

    public static void showSnapshotViewActForResult(Activity context,int requestCode, String path)
    {
        Intent intent = new Intent(context, SnapshotViewAct.class);
        intent.putExtra(Lunch.IMAGE_CHECK_FIELD_NAME,  path);
        context.startActivityForResult(intent, requestCode);
    }


    public static void showEditSumActForResult(Activity context, int indexUser, int requestCode)
    {
        Intent intent = new Intent(context, EditSumAct.class);
        intent.putExtra(EditSumAct.USER_ITEM, indexUser);
        context.startActivityForResult(intent, requestCode);
    }

    public static void showHistoryLunchesAct(Context context, int groupId)
    {
        Intent intent = new Intent(context, HistoryLunchesAct.class);
        intent.putExtra(HistoryLunchesAct.HISTORY_LUNCHES_GROUP, groupId);
        context.startActivity(intent);
    }

    public static void showMembersOfLunchAct(Context context, int lunchId)
    {
        Intent intent = new Intent(context, MembersOfLunchAct.class);
        intent.putExtra(MembersOfLunchAct.LUNCH, lunchId);
        context.startActivity(intent);
    }

    public static void showAddUserActForResult(Activity context, int requestCode, int groupId)
    {
        Intent intent = new Intent(context, AddUserAct.class);
        intent.putExtra(Group.GROUP_ID, groupId);
        context.startActivityForResult(intent, requestCode);
    }

    public static void showAboutProgramAct(Context context)
    {
        Intent intent = new Intent(context, AboutProgramAct.class);
        context.startActivity(intent);
    }

    public static void showSupportAct(Context context)
    {
        Intent intent = new Intent(context, SupportAct.class);
        context.startActivity(intent);
    }

    public static void showPreferenceAct(Context context, String preferenceActivity)
    {
        Intent intent = new Intent(context, PreferenceAct.class);
        intent.putExtra(PreferenceAct.PREFERENCE_ACTIVITY, preferenceActivity);
        context.startActivity(intent);
    }

    public static void showUserStatisticsAct(Context context, int index)
    {
        Intent intent = new Intent(context, UserStatisticsAct.class);
        intent.putExtra(User.USER_ID, index);
        context.startActivity(intent);
    }


    public static void showPersonalProfileAct(Context context, String index)
    {
        Intent intent = new Intent(context, PersonalProfileAct.class);
        intent.putExtra(UserContact.USER_CONTACT_ID_FIELD_NAME, index);
        context.startActivity(intent);
    }
}
